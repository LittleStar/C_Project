//
// Created by Star on 6/11/16.
//

//
//  First.c
//  CProject
//
//  Created by Star on 9/8/16.
//  Copyright © 2016年 Star. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <zconf.h>
#include "Second.h"
//#define SIZE 10


int C9N9_15();

int donut();

int SUM(int, int);

void alter(int *, int *);

int text();

int C9N9();

int C9N1();

void C9N2();

void Recursion();

int C9N3();

void C9N4();

void C9N5();

void C9N5_2();

int C9N6();

void C9N7();

void C9N9_1();

void fuction_1();

void fuction_2();

void fuction_3();

void fuction_4();

void fuction_5();

void fuction_6();

int C10N14();

int C10N10();

int C10N8();

int C10N10_1();

int C10N10_2();

int C10N10_4();

int C10N10_5();

int C10N10_7();

int C10N10_15();

void F();

void F1();

void F2();

void F3();

void F4();

void F5();

void C10N1();

void C10N2();

void C10N3();

void C10N4();

void C10N5();


int main_1() {
    int i, j;
    char c;
    scanf("%c", &c);
    for (i = 0; i < 5; i++) {
        for (j = 4 - i; j > 0; j--)
            printf(" ");
        for (j = 0; j <= i; j++)
            printf("%c", c + j);
        for (j = i; j > 0; j--)
            printf("%c", c + j - 1);
//            for(j=4-i;j>0;j--)
//                printf(" ");
        printf("\n");
    }
    return 0;
}

int main_2(void) {
//    C9N9_15();
//    donut();
//    SUM();
//    alter();
//    text();
//    C9N9();
//    C9N1();

//    C9N2();
//    Recursion();
//    C9N3();
//    C9N4();
//    C9N5();
//    C9N5_2();
//    C9N6();
//    C9N7();
//    C9N9_1();
//    fuction_1();
//    fuction_2();
//    fuction_3();
//    fuction_4();
//    fuction_5();
//    fuction_6();
//    C10N14();
//    C10N10();
//    C10N8();
//    C10N10_1();
//    C10N10_2();
//    C10N10_4();
//    C10N10_5();
//    C10N10_7();
//    C10N10_15();
//    F();
//    F1();
//    F2();
//    F3();
//    F4();
//    F5();
//    C10N1();
//    C10N2();
//    C10N3();
//    C10N4();
//    C10N5();




//    int i=9;
//    int*p=&i;
//    ++*p;
//    printf("%d",*p);

    printf("\n");
    return 0;
}


void interchange(int *u, int *v);

int C9N9_15() {
    int x = 5, y = 10;

    printf("Orginally x=%d and y=%d.\n", x, y);
    interchange(&x, &y);
    printf("Now x=%d and y=%d.\n", x, y);
    return 0;
}

void interchange(int *u, int *v) {
    int temp;

    temp = *u;
    *u = *v;
    *v = temp;

}

int donut() {
    int a, b;
    printf("Input a number:");
    scanf("%d", &a);

    for (b = 1; b <= a; b++)
        printf("%d", 0);

    return 0;

}


int SUM(int a, int b) {
    printf("输入两个数，求两数之和:");
    scanf("%d %d", &a, &b);
    printf("%d", a + b);
    return a + b;

}

int text() {
    int x, y;
    printf("Input 2 number:");
    scanf("%d %d", &x, &y);
    alter(&x, &y);

    return 0;

}

void alter(int *u, int *v) {

    int temp;

    temp = *u + *v;
    *v = *u - *v;
    *u = temp;
    printf("%d %d", *u, *v);

}

int C9N8(int a, int b, int c) {
    int max = a;
    if (b > max)
        max = b;
    if (c > max)
        max = c;
    return max;
}

void menu();

int getchoice(int, int);

void menu(void) {
    printf("Please choose one of the following:");
    printf("1)copy files)\t2)move files\n");
    printf("3)remove files\t4)quit");
    printf("Enter the number of your choice:");

}

int getchoice(int low, int high) {
    int num;
    scanf("%d", &num);
    while (num < low || num > high) {
        printf("%d is not a valid choice.Please try again.", num);
        menu();
        scanf("%d", &num);
    }
    return num;

}


int C9N9() {
    int a;
    menu();

    while ((a = getchoice(1, 4)) != 4)
        printf("I like choice %d.\n", a);
    printf("Bye\n");
    return 0;

}


double min(double x, double y) {

    return x > y ? y : x;
//    if(x>y)
//        printf("%f",y);
//    else
//        printf("%f",x);
//
//    return 0;
}

int C9N1() {
    double x, y;
    printf("Input 2 number:");
    scanf("%lf %lf", &x, &y);
    printf("%f", min(x, y));
    return 0;
}

void chline(char str[], int i, int j) {
    int index;
    uLong length = 0;

    length = strlen(str);
    for (index = 0; index < length; index++) {
        if (index >= i - 1 && index <= j - 1)
            printf("%c", str[index]);
    }


    return;
}

void C9N2() {
    int i, j;
    char str[25];
    printf("Input your favorite word or sentence:");
    scanf("%s", str);
    printf("请输入两个限定数，截取字符串:");
    scanf("%d %d", &i, &j);
    chline(str, i, j);

}

long Fibonacci(int n);

long FibonacciLoop(int n);

void Recursion() {
    int n;
    long a;

    printf("Input a number:");
    scanf("%d", &n);
//    a=Fibonacci(n);
    a = FibonacciLoop(n);
//    printf("%ld",Fibonacci(n));
    printf("%ld", a);

}


long Fibonacci(int n) {
    if (n == 0)
        return 0;
    if (n == 1)
        return 1;

    long result;
    if (n > 1)
        result = Fibonacci(n - 1) + Fibonacci(n - 2);
    return result;
}

long FibonacciLoop(int n) {

    long a = 0;
    long b = 1;

    if (n == 0)
        return 0;
    if (n == 1)
        return 1;

    long result = 0;
    for (int i = 2; i <= n; i++) {
        result = a + b;
        a = b;
        b = result;
    }
    return result;
}

void fuction(char ch, int x, int y) {
    for (int j = 1; j <= y; j++) {
        for (int i = 1; i <= x; i++)
            printf("%c", ch);
        printf("\n");

    }
}

int C9N3() {
    int a, b;
    char str;

    printf("Input your favorite word or sentence:");
    scanf("%c", &str);
    printf("Please input column and row:");
    scanf("%d %d", &a, &b);
    fuction(str, a, b);
    return 0;

}

double average(double a, double b) {
    double result;
    result = 1 / ((1 / a + 1 / b) / 2);

    return result;
}

void C9N4() {
    double x, y;
    printf("input 2 number of double:");
    scanf("%lf %lf", &x, &y);
//    z=average(x,y);
//    printf("%0.3lf",z);
    printf("1/((1/%lf+1/%lf)/2)=%0.3lf\n", x, y, average(x, y));
    return;

}

int ischar(char str[]) {
    u_long length;
    length = strlen(str);
    for (int index = 0; index < length; index++) {
        if ((str[index] >= 97 && str[index] <= 122) || (str[index] >= 65 && str[index] <= 90)) {
            printf("not number,is char,将该字符转换成该字符的数值位置");
            printf("%d\n", str[index]);
        } else
            printf("-1\n");

    }
    return 0;
}

void PointerArray() {
    int pa[10];
    int a;
    a = pa[6];
    int (*pp)[10]; // This is a pointer array.
    a = *pp[2];
}

int C9N6() {
    char ch[50];
    for (int i = 0; i < 50; i++) {
        ch[i] = 0;
    }
    char temp;

    printf("输入字符串:");
    scanf("%s", ch);
    scanf("%c", &temp);

    ischar(ch);
    return 0;

}

double larger_of(double *x, double *y) {
    double result;
    result = *x > *y ? *x : *y;
    *x = result;
    *y = result;
    return result;

//    if(*x>*y)
//    {
//        *y=*x;
//    }
//    else if(*x<*y)
//        *x=*y;
//    else
//    {
//        *x=*y;
//    }
//    return *x;

}

void C9N5() {
    double a, b;
    printf("Input two doubles:");
    scanf("%lf %lf", &a, &b);
    larger_of(&a, &b);
    printf("The result is:a=%0.3lf,b=%0.3lf\n", a, b);

}

double larger(double x, double y) {
    double result;
    result = x > y ? x : y;
    x = result;
    y = result;
    printf("The same:%lf %lf", x, y);
    return x;
}

void C9N5_2() {
    double a, b;
    printf("Input two doubles:");
    scanf("%lf %lf", &a, &b);
    larger(a, b);
    printf("The result is:a=%0.3lf,b=%0.3lf\n", a, b);

}


double power1(double n, int p) {
    if (n == 0)
        return 0;
    if (p == 0)
        return 1;

    double pow = 1;
    int i;
    if (p > 0) {
        for (i = 1; i <= p; i++)
            pow *= n;
    }
    if (p < 0) {
        for (i = -1; i >= p; i--)
            pow *= 1 / n;
    }
    return pow;
}


double power2(double n, int p) {
    if (n == 0)
        return 0;
    if (p == 0)
        return 1;

    double pow = 1;
    int i;
    if (p > 0) {
        for (i = 1; i <= p; i++)
            pow *= n;
    }
    if (p < 0)
        pow *= 1 / power2(n, -p);

    return pow;
}

void C9N7() {
    double x, result;
    int y;
    printf("printf a number and the integer power:");
    while (scanf("%lf%d", &x, &y) == 2) {
//        result=power1(x,y);
        result = power2(x, y);
        printf("%lf的%d次方等于%lf.\n", x, y, result);
        printf("Enter next pair of number or q to quit\n");
    }
    printf("Bye");
    return;
}

void to_bace_n(unsigned long n, unsigned int bace) {
    int r = 0;
    r = n % bace;
    if (n >= bace)
        to_bace_n(n / bace, bace);
    putchar('0' + r);
    return;

}

void C9N9_1() {
    unsigned long number;
    unsigned int bace;

    printf("Input two integer(q to quit):\n");
    while (scanf("%lu%u", &number, &bace) == 2) {

        if (bace >= 2 && bace <= 10) {
            printf("%lu's bace %u equivalent:", number, bace);
            to_bace_n(number, bace);
            putchar('\n');
            printf("Input an integer(q to quit):\n");

        } else
            printf("value of bece is between 2 and 10!Please input it again:");
    }
    printf("Done.\n");
    return;
}

void fuction_1() {

    for (int row = 1; row <= 3; row++) {
        for (int column = 1; column <= 3; column++) {
            if (row == 1)
                printf("-");
            if (row == 2)
                printf("+");
            if (row == 3)
                printf("*");
        }
        printf("\n");
    }
}

void fuction_2() {

    for (int column = 1; column <= 3; column++)
        printf("-");
    printf("\n");

    for (int column = 1; column <= 3; column++)
        printf("+");
    printf("\n");

    for (int column = 1; column <= 3; column++)
        printf("*");

}

void fuction_3() {
    char str[4] = "-+*@";
    for (int row = 1; row <= 4; row++) {
        for (int column = 1; column <= row; column++) {
            if (row == 1)
                printf("%c", str[0]);
            if (row == 2)
                printf("%c", str[1]);
            if (row == 3)
                printf("%c", str[2]);
            if (row == 4)
                printf("%c", str[3]);
        }
        printf("\n");
    }
}

void fuction_4() {
    char str[4] = "-+*@";
    for (int row = 1; row <= 4; row++) {
        if (row % 2 == 0) {
            for (int column = 1; column <= 4; column++) {
                if (row == 2)
                    printf("%c", str[1]);
                if (row == 4)
                    printf("%c", str[3]);
            }
            printf("\n");
        } else {
            for (int column = 1; column <= 2; column++) {
                if (row == 1)
                    printf("%c", str[0]);
                if (row == 3)
                    printf("%c", str[2]);
            }
            printf("\n");
        }
    }
}


void fuction_5() {
    int array[5], *pti;
    pti = array;
    for (int index = 0; index < 5; index++) {
        array[index] = index + 9;
    }
    for (int index = 0; index < 5; index++)
        printf("%d\n", array[index]);

    printf("Array[0]=%d\n", array[0]);
    printf("*array=%d\n", *array);
    printf("*ptr=%d\n", *pti);
}


void fuction_6() {
    char letter;
    int i, j, row;
    printf("Input a capital:");
    scanf("%c", &letter);
    row = letter - 'A' + 1;

    for (i = 0; i < row; i++) {
        for (j = 1; j < row - i; j++)
            printf(" ");
        for (j = 0; j <= i; j++)
            printf("%c", 'A' + j);
        for (j = i; j > 0; j--)
            printf("%c", 'A' + j - 1);
        printf("\n");

    }

}

void show_array(const double ar[], int n);

void mult_array(double ar[], int n, double mult);

int C10N14() {
    const int SIZE = 5;
    double dip[SIZE] = {20.0, 17.66, 8.2, 15.3, 22.22};
    printf("The original dip array:\n");
    show_array(dip, SIZE);
    mult_array(dip, SIZE, 2.5);
    printf("The dip array after calling mult_array():\n");
    show_array(dip, SIZE);
    return 0;
}

void show_array(const double *ar, int n) {
    int i;
    for (i = 0; i < n; i++)
        printf("%8.3f ", ar[i]);
    putchar('\n');
}

void mult_array(double *ar, int n, double mult) {
    int i;
    for (i = 0; i < n; i++)
        ar[i] *= mult;
}

int sum(int ar[], int n);

int C10N10(void) {
    const int SIZE = 10;
    int marbles[SIZE] = {20, 10, 5, 39, 4, 16, 19, 26, 31, 20};
    long answer;
    answer = sum(marbles, SIZE);
    printf("The total number of marbles is %ld.\n", answer);
    printf("The size of number is %zd bytes.\n", sizeof marbles);
//    printf("%p\n",marbles);
//    int *pp=marbles;
//    printf("%p",pp);
//    printf("%p",&pp);
    return 0;
}

int sum(int *ar, int n) {
    int i;
    int total = 0;
    for (i = 0; i < n; i++)
        total += ar[i];

    printf("%d", *ar);
    printf("The size of ar is %lu bytes.\n", sizeof ar);
    return total;
}

int C10N8() {
    int SIZE = 4;
    short dates[SIZE];
    short *pti;
    short index;
    double bills[SIZE];
    double *ptf;

    pti = dates;
    ptf = bills;
    printf("%23s %10s\n", "short", "double");
    for (index = 0; index < SIZE; index++)
        printf("pointers + %d:%p %p\n", index, pti + index, ptf + index);
    return 0;

}

int C10N10_1() {
    const int months = 12;
    int days[months] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int index;
    for (index = 0; index < months; index++)
        printf("Month %d has %2d days. \n", index + 1, days[index]);
//    printf("Month %d has %2d days. \n",index+1,*(days+index));
    return 0;
}

int C10N10_2() {
    const int SIZE = 4;
    int no_data[SIZE] = {12, 13};
    int i;

    printf("%2s%14s\n", "i", "no_data[i]");
    for (i = 0; i < SIZE; i++)
        printf("%2d%14d\n", i, no_data[i]);
    return 0;

}

int C10N10_4() {
    int days[] = {31};
    int index;
    for (index = 0; index < sizeof days; index++)
        printf("Month %d has %2d days. \n", index + 1, days[index]);
    printf("%lu", sizeof days);
    return 0;
}

//int C10N10_5()
//{
//    const int SIZE = 5;
//    int oxen[SIZE] = {5,3,2,8};
//    int yaks[SIZE];
//
//    yaks=oxen[0];
//    printf("%d",yaks[0]);
//    return 0;
//}

int C10N10_7() {
    const int months = 12;
    const int years = 5;
    const int rain[years][months] = {
            {1, 2, 3, 4, 5, 6,  7,  8,  9,  10, 11, 12},
            {2, 3, 4, 5, 6, 7,  8,  9,  10, 11, 12, 13},
            {3, 4, 5, 6, 7, 8,  9,  10, 11, 12, 13, 14},
            {4, 5, 6, 7, 8, 9,  10, 11, 12, 13, 14, 15},
            {5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}
    };
    int year, month;
    int subtot, total;
    printf("Year rainfall (inches)\n");
    for (year = 0, total = 0; year < years; year++) {
        for (month = 0, subtot = 0; month < months; month++)
            subtot += rain[year][month];
        printf("%5d %15d\n", 2000 + year, subtot);
        total += subtot; //所有年度的总降水量
    }
    printf("\nThe yearly average is %d inches.\n\n", total / years);
    printf("Monthly averages:\n");
    printf("Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec\n");

    for (month = 0; month < months; month++) {
        for (year = 0, subtot = 0; year < years; year++)
            subtot += rain[year][month];
        printf("%4d", subtot / years);

    }
    printf("\n");
    return 0;


}


int C10N10_15() {
    int zippo[4][2] = {{2, 4},
                       {6, 8},
                       {1, 3},
                       {5, 7}};
    printf("zippo = %p,  zippo + 2 = %p\n", zippo, zippo + 2);

    printf("zippo[0] = %p,  zippo[0] + 1 = %p\n", zippo[0], zippo[0] + 1);

    printf("*zippo = %p,  *zippo + 1 = %p\n", *zippo, *zippo + 1);
    printf("*zippo = %p,  *(*zippo + 3) = %d\n", *zippo, *(*zippo + 3));
    printf("zippo[0][0] = %d\n", zippo[0][0]);
    printf("*zippo[0] = %d\n", *zippo[0]);
    printf("**zippo = %d\n", **zippo);
    printf("zippo[2][1] = %d\n", zippo[2][1]);
    printf("zippo[1][1] = %d\n", zippo[1][1]);
    printf("*(*(zippo+2)+1) = %d\n", *(*(zippo + 2) + 1));
    printf("*(zippo+2)+1 = %p\n", *(zippo + 2) + 1);
    printf("*(zippo+2) = %p\n", *(zippo + 2));
    printf("*(*(zippo + 2)) = %d\n", *(*(zippo + 2)));

    int p[4] = {1, 2, 3, 4};
    printf("p=%p,p+1=%p\n", p, p + 1);
    printf("p[0]=%d, p[0]+1=%d\n", p[0], p[0] + 1);
    printf("*p=%d,*p+1=%d\n", *p, *p + 1);


    return 0;
}

/*
// typedef
void TypeAndClass()
{
    typedef int number;

    int a,b,num;
    number c,d,n;


    typedef int arr4[4];
    int array[4];
    arr4 ar;
    arr4 br[3];
    int cr[3][4];

    typedef int arr3x4[3][4];
    arr3x4 cool;
    int array3x4[3][4];
}

void VLA_Func(int col,int ar[][col])
{

}
void VLA_Func2(int (*ar)[4][5])
{

}

void VLA()
{
    int a[5][4];
    int b[3][2];

    VLA_Func(4, a);
    VLA_Func(2, b);

    int (*p)[4];
}

struct Structure
{
//    Structrue()
//    {
//        row=5;
//        array[row]={1,2,3,4,5};
//    }

    int row,col;
//    int array[row];
    int length;
    int length2;

//    int GetLength()
//    {
//        return sizeof array;
//    }
};

void Compound()
{
    int a[2]={1,2};
    int b[3][2] =
        {
        {1,2},
        {2,3},
        {3,5}};

    VLA_Func(2, b);

    VLA_Func(2, (int[3][2])
            {{1,2},
            {2,3},
            {3,5}});

    b[0][0]=1;


    struct Structure qqq;
    qqq.length = 3;
    qqq.length2=9;
//    qqq.GetLength();

    struct Structure aa;
    aa.length =3;
//    aa.GetLength();

}

void StructFunc(struct Structure ss)
{

}
*/

void F() {
    int *ptr;
    int torf[2][2] = {12, 14, 16};
    ptr = torf[0];
    printf("a\n");
    printf("%d\n", *ptr);
    printf("%d\n", *ptr + 1);
    printf("%d\n", *ptr + 2);
    printf("%d\n", *ptr + 3);
    printf("%d\n", *(ptr + 2));


}

void F1() {
    int *ptr;
    int fort[2][2] = {{12},
                      {14, 16}};
    ptr = fort[0];
    printf("b\n");
    printf("%d\n", *ptr);
    printf("%d\n", *ptr + 1);
    printf("%d\n", *ptr + 2);
    printf("%d\n", *ptr + 3);
    printf("%d\n", *(ptr + 2));

}

void F2() {
    int (*ptr)[2];
    int torf[2][2] = {12, 14, 16};
    ptr = torf;
    printf("c\n");
    printf("%d\n", **ptr);
    printf("%d\n", **ptr + 1);
    printf("%d\n", **ptr + 2);
    printf("%d\n", **ptr + 3);
    printf("%d\n", **(ptr + 2));

}

void F3() {
    int (*ptr)[2];
    int fort[2][2] = {{12},
                      {14, 16}};
    ptr = fort;
    printf("d\n");
    printf("%d\n", **ptr);
    printf("%d\n", **ptr + 1);
    printf("%d\n", **ptr + 2);
    printf("%d\n", **ptr + 3);
    printf("%d\n", **(ptr + 2));
//    show((int[4]){8,3,9,2},4);
}

void F4() {
    int fort[2][2] = {{12},
                      {14, 15}};
    int (*ptr)[2];
    printf("%d\n", fort[1][1]);
    ptr = fort;
    printf("%p\n", ptr);
    printf("%p\n", &ptr);
    printf("%p\n", *ptr);

    printf("%p\n", &fort);
    printf("%p\n", fort);
    printf("%p\n", fort[0]);
    printf("%p\n", &fort[0]);
    printf("%p\n", &fort[0][0]);
    printf("%p\n", *fort);

    printf("%d\n", fort[0][0]);
    printf("%d\n", *fort[0]);


}

void F5() {
    int rain[4][3] = {{9},
                      {0, 2, 1}};
//    int rain[4][3]={{9,2,3},{0,2,1},{4,5,6},{8,9,0}};
    int (*p)[3];
    p = rain;

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 3; j++)
            printf("%d\n", rain[i][j]);
    }
    printf("~~~~~~~~~~\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%d\n", *(*(p + i) + j));
        }
    }
    printf("~~~~~~~~~~\n");
    int (*pp)[3] = rain;
    for (int i = 0; i < 4 * 3; i++) {
        printf("%d\n", **(pp + i));
    }
    printf("~~~~~~~~~~\n");

    int *ptr;
    int abc = 3, num = 9;
    ptr = &num;
    ptr = &abc;
    *ptr = 88;
    printf("%d\n", num);
}

void C10N1() {
    const int MONTHS = 12;
    const int YEARS = 5;
    int rain[YEARS][MONTHS] = {
            {1, 2, 3, 4, 5, 6,  7,  8,  9,  10, 11, 12},
            {2, 3, 4, 5, 6, 7,  8,  9,  10, 11, 12, 13},
            {3, 4, 5, 6, 7, 8,  9,  10, 11, 12, 13, 14},
            {4, 5, 6, 7, 8, 9,  10, 11, 12, 13, 14, 15},
            {5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}
    };
    int year, month, subtot, total;
    int (*p)[12];
    p = rain;
    printf("每年各月份的总降水量:\n");
    for (year = 0, total = 0; year < YEARS; year++) {
        for (month = 0, subtot = 0; month < MONTHS; month++)
            subtot += *(*(p + year) + month);
        printf("%d %d\n", 2000 + year, subtot);
        total += subtot;
    }
    printf("年平均降水量为%d\n", total / year);
    printf("每月各年份的总降水量:\n");
    printf("Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec\n");
    for (month = 0; month < MONTHS; month++) {
        for (year = 0, subtot = 0; year < YEARS; year++)
            subtot += *(*(p + year) + month);
        printf("%d ", subtot / year); //每年一月份平均降水量

    }
    printf("\n");
    return;
}


const int MONTHS = 12;
const int YEARS = 5;

void display_1(int (*P)[MONTHS], int years);

void C10N1_1() {

    int rain[YEARS][MONTHS] = {
            {1, 2, 3, 4, 5, 6,  7,  8,  9,  10, 11, 12},
            {2, 3, 4, 5, 6, 7,  8,  9,  10, 11, 12, 13},
            {3, 4, 5, 6, 7, 8,  9,  10, 11, 12, 13, 14},
            {4, 5, 6, 7, 8, 9,  10, 11, 12, 13, 14, 15},
            {5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}
    };
    display_1(rain, YEARS);
}

void display_1(int (*p)[MONTHS], int years) {
    int year, month, subtot, total;
//    int (*p)[12];
//    p=rain;
    printf("每年各月份的总降水量:\n");
    for (year = 0, total = 0; year < YEARS; year++) {
        for (month = 0, subtot = 0; month < MONTHS; month++)
            subtot += *(*(p + year) + month);
        printf("%d %d\n", 2000 + year, subtot);
        total += subtot;
    }
    printf("年平均降水量为%d\n", total / year);
    printf("每月各年份的总降水量:\n");
    printf("Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec\n");
    for (month = 0; month < MONTHS; month++) {
        for (year = 0, subtot = 0; year < YEARS; year++)
            subtot += *(*(p + year) + month);
        printf("%d ", subtot / year); //每年一月份平均降水量

    }
    printf("\n");
    return;
}


void copy_arr(double[], double[], int);

void copy_ptr(double *, double *, int);

void C10N2() {
    double source[5] = {1.1, 2.2, 3.3, 4.4, 5.5};
    double target1[5] = {0};
    double target2[5] = {0};
    printf("Before operation\n");
    printf("source:\t%f\t%f\t%f\t%f\t%f\n", source[0], source[1], source[2], source[3], source[4]);
    printf("target1:\t%f\t%f\t%f\t%f\t%f\n", target1[0], target1[1], target1[2], target1[3], target1[4]);
    printf("target2:\t%f\t%f\t%f\t%f\t%f\n", target2[0], target2[1], target2[2], target2[3], target2[4]);

    copy_arr(source, target1, 5);
    copy_ptr(source, target2, 5);

    printf("After operation\n");
    printf("source:\t%f\t%f\t%f\t%f\t%f\n", source[0], source[1], source[2], source[3], source[4]);
    printf("target1:\t%f\t%f\t%f\t%f\t%f\n", target1[0], target1[1], target1[2], target1[3], target1[4]);
    printf("target2:\t%f\t%f\t%f\t%f\t%f\n", target2[0], target2[1], target2[2], target2[3], target2[4]);

    return;

}

void copy_arr(double a1[], double a2[], int n) {
    int i;
    for (i = 0; i < n; i++)
        a2[i] = a1[i];
}

void copy_ptr(double *p1, double *p2, int n) {
    int i;
    for (i = 0; i < n; i++)
        *(p2 + i) = *(p1 + i);

}

int fuc(int a[], int n);

void C10N3() {
    const int width = 5;
    int array[] = {1, 2, 3, 4, 8};
    printf("The max is :%d\n", fuc(array, width));

}

int fuc(int a[], int n) {
    int i, max;
    for (i = 0, max = a[0]; i < n; i++) {
        if (max < a[i])
            max = a[i];
    }
    return max;
}

int fuc1(int a[], int n);

void C10N4() {
    const int width = 6;
    int array[] = {4, 5, 6, 7, 8, 9};
    printf("The max numnber's index is :%d\n", fuc1(array, width));

}

int fuc1(int a[], int n) {
    int i, max;
    for (i = 0, max = 0; i < n; i++)
        if (a[max] < a[i])
            max = i;
    return max;
}

int fuc2(int a[], int n);

void C10N5() {
    const int width = 4;
    int array[] = {1, 2, 3, 664};
    printf("最大值和最小值的差是%d\n", fuc2(array, width));

}

int fuc2(int a[], int n) {
    int i, max, min;
    max = min = a[0];
    for (i = 0; i < n; i++) {
        if (max < a[i])
            max = a[i];
        if (min > a[i])
            min = a[i];
    }
    printf("max=%d min=%d\n", max, min);

    return max - min;
}


//~~~~~~~~~~~~~~~
