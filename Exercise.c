//
// Created by Star on 10/11/16.
//

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include<math.h>

//~~~~~~~~~~~~~~~~~~~~~
#define M 8*sizeof(int)+1

char *extend_1(char *destination, char *source);

char *reverse_1(char *destination, char *source);

char *and_1(char *destination, char *source1, char *source2);

char *or_1(char *destination, char *source1, char *source2);

char *exclusive_or_1(char *destination, char *source1, char *source2);


int P444_2(int argc, char *argv[]) {
    char x[M], y[M], z[M];

    printf("  x=%s\n", extend_1(x, argv[1]));
    printf("  y=%s\n", extend_1(y, argv[2]));
    printf(" ~x=%s\n", reverse_1(z, x));
    printf(" ~y=%s\n", reverse_1(z, y));
    printf("x&y=%s\n", and_1(z, x, y));
    printf("x|y=%s\n", or_1(z, x, y));
    printf("x^y=%s\n", exclusive_or_1(z, x, y));
    return 0;
}

char *extend_1(char *destination, char *source) {
    int i;
    for (i = 0; i < M - 1 - strlen(source); i++)
        destination[i] = '0';
    destination[i] = '\0';
    strcat(destination, source);
    return destination;
}

char *reverse_1(char *destination, char *source) {
    char *save = destination;
    strcpy(destination, source);
    while (*destination != '\0') {
        if (*destination == '0')
            *destination = '1';
        else
            *destination = '0';
        destination++;
    }
    return save;
}

char *and_1(char *destination, char *source1, char *source2) {
    char *save = destination;
    while (*source1 != '\0') {
        if (*source1 == '1' && *source2 == '1')
            *destination = '1';
        else
            *destination = '0';
        source1++;
        source2++;
        destination++;
    }
    return save;
}

char *or_1(char *destination, char *source1, char *source2) {
    char *save = destination;
    while (*source1 != '\0') {
        if (*source1 == '1' || *source2 == '1')
            *destination = '1';
        else
            *destination = '0';
        source1++;
        source2++;
        destination++;
    }
    return save;
}

char *exclusive_or_1(char *destination, char *source1, char *source2) {
    char *save = destination;
    while (*source1 != '\0') {
        if (*source1 != *source2)
            *destination = '1';
        else
            *destination = '0';
        source1++;
        source2++;
        destination++;
    }
    return save;
}
//~~~~~~~~~~~~~~~~~~~~~~

void P150_12() {
    int number[8];
    int i;
    for(i=0;i<8;i++) {
        number[i]=pow(2,i);
        printf("%d ",number[i]);
    }
    printf("\n");
    i=0;
    do
        printf("%d ",number[i]);
    while(++i<8);
}
//~~~~~~~~~~~~~~~~~~~~~~~~~
void P186_1() {
    char a;
    int space,newline,others;
    printf("Please input a string end by #:");
    while((a=getchar())!='#') {
        if(a==' ')
            space++;
        else if(a=='\n')
            newline++;
        else
            others++;
    }
    printf("%d %d %d",space,newline,others);
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~
void P186_2() {
    char a;
    int i;
    printf("Please enter a string end by'#':");
    for(i=1;(a=getchar())!='#';i++) {
        printf("%c--%d ",a,a);
        if(i%8==0)
            printf("\n");
    }
}
//~~~~~~~~~~~~~~~~~~~~
void P187_4() {
    int a=0,b=0;
    char ch;
    printf("Please enter a string end by '#':");
    while((ch=getchar())!='#') {
        if (ch == '.') {
            putchar('!');
            a++;
        } else if (ch == '!') {
            putchar('!');
            putchar('!');
            b++;
        } else
            putchar(ch);
    }
    printf("\nthe times of'.'replaced by'!':%d\n",a);
    printf("\nthe times of'!'replaced by'!!':%d\n",b);
}
//~~~~~~~~~~~~~~~~~~~~~~
void P187_6() {
    int count=0;
    char former=0,present;
    printf("Please enter a word:");
    while((present=getchar())!='#') {
        if((former=='e') && (present=='i'))
            count++;
        former=present;
    }
    printf("ei has appeared %d times",count);
}
//~~~~~~~~~~~~~~~~~~~~
void chline1(char,int,int);
void P243_2() {
    char ch;
    int x,y;
    printf("Please input a char:");
    scanf("%c",&ch);
    printf("Please input two number:");
    scanf("%d %d",&x,&y);
    chline1(ch,x,y);
}
void chline1(char ch,int i,int j) {
    int k;
    for(k=0;k<i;k++)
        printf("");
    for(;k<=j;k++)
        printf("%c",ch);
    printf("\n");
}
//~~~~~~~~~~~~~~~~~~~
void to_base(unsigned long,unsigned int);
void P243_9() {
    unsigned long number;
    unsigned int base;
    printf("Enter an integer(q to quit):");
    while(scanf("%lu %u",&number,&base)==2) {
        printf("%lu's base equivalent:",number);
        to_base(number,base);
        printf("\nEnter an integer(q to quit):");
    }
    printf("Done!");

}
void to_base(unsigned long n,unsigned int base)
{
    unsigned int r;
    r=n%base;
    if(n>=base)
        to_base(n/base,base);
    putchar('0'+ r );
}
//~~~~~~~~~~~~~~~~~~
long Fibonacci_1(int);
void P243_10() {
    unsigned int n;
    printf("请输入一个整数（Q退出）:");
    while(scanf("%d",&n)==1) {
        printf("1～%d的斐波那契数列的计算是%ld",n,Fibonacci_1(n));
        printf("\n请输入一个整数（Q退出）");
    }
    printf("Done!");
}

long Fibonacci_1(int n) {
    unsigned int n1,n2,i,temp;
    if(n>2) {
        for(n1=1,n2=1,i=3;i<=n;i++) {
            temp=n1+n2;
            n1=n2;
            n2=temp;
        }
    }
    else
        n2=1;

    return n2;
}
//~~~~~~~~~~~~~~~~~~~~~~
int max_number(int a[], int n);
void P281_3() {
    const int width=6;
    int array[]={2,5,6,8,9,13};
    printf("The max is %d\n",max_number(array,width));

}
//int max_number(int a[],int n) {
//    int i,max;
//    for(i=1,max=a[0];i<n;i++) {
//        if(max<a[i])
//            max=a[i];
//    }
//    return max;
//}
int max_number(int a[],int n) {
    int i,max;
    for(i=1,max=0;i<n;i++) {
        if(a[max]<a[i])
            max=i;
    }
    return max;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~
float gap(float [],int);
void P281_5() {
    const int width_1=6;
    float array[]={4.2, 5.3, 8.9, 5.4, 9.8, 3.4};
    printf("The gap between max and min is:%f\n",gap(array,width_1));

}
float gap(float a[],int n) {
    float max,min;
    int i;
    for(i=1,max=a[0],min=a[0];i<n;i++) {
        if(max<a[i])
            max=a[i];
        if(min>a[i])
            min=a[i];
    }
    return max-min;
}
//~~~~~~~~~~~~~~~~~~~~~~~
#define rows 2
#define cols 3
double copy_arr_1(double source[][cols],double target[][cols]);

void P281_6() {
    int i,j;
    double source[rows][cols]={{1,2,3},{5,4,6}};
    double target[rows][cols]={0};
    copy_arr_1(source,target);
    for(i=0;i<rows;i++) {
        for(j=0;j<cols;j++) {
            printf("%f ",target[i][j]);
        }
    }
}

double copy_arr_1(double source[][cols],double target[][cols]) {
    int i,j;
    for(i=0;i<rows;i++) {
        for(j=0;j<cols;j++)
            target[i][j]=source[i][j];
    }
}
//~~~~~~~~~~~~~~~~~~~~~
char showmenu_1(void);
void eatline_1(void);
void ToUpper_1(char *);
void ToLower_1(char *);
void Transpose_1(char *);
void Dummy_1(char *);
void show_1(void (*pf)(char *),char *);

int P415() {
    char line[81];
    char copy[81];
    char choice;
    void(*pfun)(char *); //指针指向一个函数，该函数接受一个char *参数，并且没有返回值
    puts("Enter a string(empty line to quit):");
    while(gets(line)!=NULL && line[0]!='\0') {
        while((choice=showmenu_1())!='n') {
            switch(choice) {
                case 'u':pfun = ToUpper_1;
                    break;
                case 'l':pfun = ToLower_1;
                    break;
                case 't':pfun = Transpose_1;
                    break;
                case 'o':pfun = Dummy_1;
                    break;
            }
            strcpy(copy,line);
            show_1(pfun,copy);
        }
        puts("Enter a string(empty line to quit):");
    }
    puts("Bye!");
    return 0;

}
char showmenu_1(void) {
    char ans;
    puts("Enter menu choice:");
    puts("u)uppercase l)lowercase t)transposed case o)original case n)next string");
    ans = getchar(); //获取用户响应
    ans = tolower(ans); //转换为小写
    eatline_1();
    while(strchr("ulton",ans)==NULL) {
        puts("Please enter a u,l,t,o,or n:");
        ans=tolower(getchar());
        eatline_1();
    }
    return ans;
}
void eatline_1(void) {
    while(getchar()!='\n')
        continue;
}
void ToUpper_1(char *str) {
    while(*str) { //直到遇到\0（ASCII码0即NULL）0=false 1=true
        *str=toupper(*str);
        str++;
    }
}
void ToLower_1(char *str) {
    while(*str) {
        *str=tolower(*str);
        str++;
    }
}
void Transpose_1(char *str) {
    while(*str) {
        if(islower(*str))
            *str=toupper(*str);
        else if(isupper(*str))
            *str=tolower(*str);
        str++;
    }
}
void Dummy_1(char *str) {

}
void show_1(void (*pf)(char *),char * str) {
    (*pf)(str);
    puts(str);
}
//~~~~~~~~~~~~~~~~~
int STRCHR(){
    char *s = "0123456789012345678901234567890";
    char *p;
    p = strchr(s, '5');
    printf("%ld\n", s);
    printf("%ld\n", p);
    return 0;
}
//~~~~~~~~~~~~~~~~~
int days_2(int,int,int);
int leapyear_2(int);

struct month_2 {
    char name[10];
    char abbrev[4];
    int days;
    int monthnumber;
};

struct month_2 months_2[12] = {
    {"January",   "Jan", 31, 1},
    {"Fabruary",  "Fab", 28, 2},
    {"March",     "Mar", 31, 3},
    {"April",     "Apr", 30, 4},
    {"May",       "May", 31, 5},
    {"June",      "Jun", 30, 6},
    {"July",      "Jul", 31, 7},
    {"August",    "Aug", 31, 8},
    {"September", "Sep", 30, 9},
    {"October",   "Oct", 31, 10},
    {"November",  "Nov", 30, 11},
    {"December",  "Dec", 31, 12}
};

void P421() {
    int day=1,mon=12,year=1,daytotal_2;
    printf("Enter the number of day,month,year:");
    while(scanf("%d %d %d",&day,&mon,&year)==3) {
        daytotal_2=days_2(day,mon,year);
        if(daytotal_2>0)
            printf("截止到%d %d %d这一天的总天数是%d\n",day,mon,year,daytotal_2);
        else
            printf("您输入的%d %d %d为无效的输入\n",day,mon,year);
        printf("Next month(q to quit)");
    }
    printf("bye");
}

int days_2(int day,int mon,int year) {
    int i, total;
    if (leapyear_2(year))
        months_2[1].days = 29;
    else
        months_2[1].days = 28;
    if (mon < 1 || mon > 12 || day < 1 || day > months_2[mon - 1].days)
        return -1;
    else {
        for (i = 0, total = 0; i < mon - 1; i++)
            total += months_2[i].days;

    }
    return (total + day);
}


int leapyear_2(int year) {
    if(year%400==0)
        return 1;
    else if(year%4==0 && year%100!=0)
        return 1;
    else
        return 0;
}