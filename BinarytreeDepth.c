//
// Created by Star on 9/5/17.
//
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdlib.h>
#define MAXSIZE 50

#define Way1
//#define Way2

typedef struct BiTNode {
    char data;
    struct BiTNode *lchild, *rchild;
} BiTN, *PtrBiT;


#ifdef Way1
void CreateBiTree(PtrBiT *);
PtrBiT CreateBiTree2();
void visit(char, int);
void Traverse(PtrBiT pbt, int);
void Traverse2(PtrBiT root, int);

int BinarytreeDepth() {
    int level = 1;
    PtrBiT root;
    printf("请按照前序遍历的方式依次输入数据:");

//    CreateBiTree(&root);
    root=CreateBiTree2();

//    Traverse(root, level);
    Traverse2(root, level);
    return 0;
}

void CreateBiTree(PtrBiT *pbt) {
    char c;
    scanf("%c",&c);
    if(c == ' ' || c == '\n')
        return;
    else {
        (*pbt) = malloc(sizeof(BiTN));
        (*pbt)->data = c;
        CreateBiTree(&(*pbt)->lchild);
        CreateBiTree(&(*pbt)->rchild);
    }
}

PtrBiT CreateBiTree2(){
    char c;
    scanf("%c", &c);
    PtrBiT pbt = NULL;
    if(c == ' ' || c == '\n')
        return pbt;
    else {
        pbt = malloc(sizeof(BiTN));
        pbt->data = c;
        pbt->lchild = CreateBiTree2();
        pbt->rchild = CreateBiTree2();
    }
    return pbt;
}

void visit(char c, int level) {
    printf("%c 位于第 %d 层\n",c,level);
}

void Traverse(PtrBiT pbt, int level) {
    if (pbt) {
        visit(pbt->data, level);
//        printf("%c ", pbt->data);
        Traverse(pbt->lchild, level + 1);
        Traverse(pbt->rchild, level + 1);

    }
    return;
}

void Traverse2(PtrBiT root, int level) {
    if (root == NULL) {
        return;
    }
    PtrBiT s[MAXSIZE];
    int front, rear;
    front = rear = -1;

    s[++rear] = root; // 根指针入队
    visit(root->data, level);
    level++;

    while( front != rear) {
        PtrBiT ptemp = s[++front];
        printf("%c ",ptemp->data);

        if(ptemp->lchild) {
            s[++rear] = ptemp->lchild;
//            visit(ptemp->lchild->data, level);
        }
        if(ptemp->rchild) {
            s[++rear] = ptemp->rchild;
//            visit(ptemp->rchild->data, level);
        }
        level++;
    }
    return;
}


#endif


#ifdef Way2

void CreateBiTree(PtrBiT *, char *, int *);

void visit(char, int);

void Traverse(PtrBiT pbt, int);


void visit(char c, int level) {
    printf("%c 位于第 %d 层\n", c, level);
}

int BinarytreeDepth() {
    int length = 0;
    char c, array[100];
    int level = 1;

    printf("请按照前序遍历的方式依次输入数据:");
    while (c = getchar()) {
        if (c == '\n') {
            break;
        }
        array[length] = c;
        length++;
    }

    PtrBiT root;
    int index = 0;
    CreateBiTree(&root, array, &index);
    Traverse(root, level);

    return 0;
}

void CreateBiTree(PtrBiT *pbt, char *array, int *index) {
    char c;
    c = array[*index];

    if (c == ' ') {
        (*index)++;
        return;
    } else if (c == '\0') {
        return;
    } else {
        *pbt = malloc(sizeof(BiTN));
        (*pbt)->data = c;
        (*index)++;
        CreateBiTree(&(*pbt)->lchild, array, index);
        CreateBiTree(&(*pbt)->rchild, array, index);
    }
}

void Traverse(PtrBiT pbt, int level) {
    if (pbt) {
//        printf("%c ", pbt->data);
        visit(pbt->data, level);
        Traverse(pbt->lchild, level + 1);
        Traverse(pbt->rchild, level + 1);
    }
    return;
}

#endif



