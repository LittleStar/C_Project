//
// Created by Star on 5/11/16.
//

//
//  Second.c
//  CProject
//
//  Created by Star on 19/9/16.
//  Copyright © 2016年 Star. All rights reserved.
//
#include "Second.h"

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdlib.h>

//void copy_arr2(double[], double[], int);
//
//void copy_ptr2(double *, double *, int);
//
//void C10N2_2() {
//    double source[5] = {1.1, 2.2, 3.3, 4.4, 5.5};
//    double target1[5] = {0};
//    double target2[5] = {0};
//    printf("Before operation\n");
//    printf("source:\t%f\t%f\t%f\t%f\t%f\n", source[0], source[1], source[2], source[3], source[4]);
//    printf("target1:\t%f\t%f\t%f\t%f\t%f\n", target1[0], target1[1], target1[2], target1[3], target1[4]);
//    printf("target2:\t%f\t%f\t%f\t%f\t%f\n", target2[0], target2[1], target2[2], target2[3], target2[4]);
//
//    copy_arr2(source, target1, 5);
//    copy_ptr2(source, target2, 5);
//
//    printf("After operation\n");
//    printf("source:\t%f\t%f\t%f\t%f\t%f\n", source[0], source[1], source[2], source[3], source[4]);
//    printf("target1:\t%f\t%f\t%f\t%f\t%f\n", target1[0], target1[1], target1[2], target1[3], target1[4]);
//    printf("target2:\t%f\t%f\t%f\t%f\t%f\n", target2[0], target2[1], target2[2], target2[3], target2[4]);
//
//    return;
//
//}
//
//void copy_arr2(double a1[], double a2[], int n) {
//    int i;
//    for (i = 0; i < n; i++)
//        a2[i] = a1[i];
//}
//
//void copy_ptr2(double *p1, double *p2, int n) {
//    int i;
//    for (i = 0; i < n; i++)
//        *(p2 + i) = *(p1 + i);
//
//}
//
//void copyArr(int a1[], int a2[], int n) {
//    int i;
//    for (i = 0; i < n; i++)
//        a2[i] = a1[i];
//}
//
//void copyPtr(int *a1, int *a2, int n) {
//    int i;
//    for (i = 0; i < n; i++)
//        *(a2 + i) = *(a1 + i);
//}
//
//void C10N6() {
//    int array[2][3] = {
//            {3, 4, 1},
//            {1, 9, 77},
//    };
//
//    int brr[2][3] = {
//            {0}
//    };
//
//    for (int i = 0; i < 2; i++)
//        copyArr(array[i], brr[i], 3);
//
//    printf("After operation\n");
//    printf("array:\t%d\t%d\t%d\n", array[2][0], array[2][1], array[2][2]);
//    printf("brr:\t%d\t%d\t%d\n", brr[2][0], brr[2][1], brr[2][2]);
//
//
//}
//
//void C10N6_2() {
//    int array[2][3] = {
//            {3, 4, 1},
//            {1, 9, 77},
//    };
//
//    int brr[2][3] = {
//            {0}
//    };
//
//
//    for (int i = 0; i < 2; i++)
//        for (int j = 0; j < 3; j++)
//            brr[i][j] = array[i][j];
//}
//
//void Add(int a[], int b[], int c[], int n) {
//    int i;
//    for (i = 0; i < n; i++)
//        c[i] = a[i] + b[i];
//
//}
//
//void C10N9_1() {
//    int array[4] = {2, 4, 5, 8};
//    int array_1[4] = {1, 0, 4, 6};
//    int array_2[4] = {0};
//    Add(array, array_1, array_2, 4);
//    printf("%d %d %d %d\n", array_2[0], array_2[1], array_2[2], array_2[3]);
//
//}
//
//const int display_const_12 = 5;
//
//void display(int (*p)[display_const_12], int n) {
//
//    int i, j;
//    for (i = 0; i < n; i++)
//        for (j = 0; j < display_const_12; j++)
//            printf("p[i][j]=%d\t", p[i][j]);
//    printf("\n");
//
//}
//
//void again(int (*p1)[display_const_12], int n) {
//    int i, j;
//    for (i = 0; i < n; i++)
//        for (j = 0; j < display_const_12; j++)
//            p1[i][j] += p1[i][j];
////            p1[i][j]=2 * p1[i][j];
//    printf("\n");
//
//}
//
//void C10N101() {
//    int array[3][5] = {
//            {0, 1, 2, 3, 4},
//            {1, 2, 3, 0, 1},
//            {2, 0, 1, 2, 3}
//    };
//    display(array, 3);
//    again(array, 3);
//    display(array, 3);
//    again(array, 3);
//    display(array, 3);
//}
//
//#define COLS 5
//
//void store(double p[][COLS], int row) {
//    int i = 0, j = 0;
//    printf("Input 3 arrays of 5 numbers:");
//
//    for (i = 0; i < row; i++)
//        for (j = 0; j < COLS; j++) {
//            scanf("%lf", &p[i][j]);
//            printf("p[i][j]=%lf\t", p[i][j]);
//        }
//
//}
//
//void C10N12_1() {
//    double array[3][COLS];
//    store(array, 3);
//
//}
//
//struct book {
//    char title[40];
//    char author[40];
//    float price;
//};
//
//void manybook() {
//    struct book cprimer = {"TYP", "I love Baobao"};
//    char a[40] = "WYJ";
//    char b[40] = "C Primer";
//    cprimer.price = 9999999;
//}
//
//void Modify(struct book *);
//
//void ChangeBook() {
//    struct book bk;
//    bk.price = 1;
//    Modify(&bk);
//    printf("%f", bk.price);
//}
//
//void Modify(struct book *a) {
//    (*a).price = 999;
//}
//
//struct value {
//    int a;
//    int b;
//};
//
//struct value getinfo() {
//    struct value temp;
//    temp.a = 9;
//    temp.b = 99;
//    printf("Temp addr: %p\n", &temp);
//    return temp;
//}
//
//void C14Example() {
//    struct value first;
//    printf("First addr: %p\n", &first);
//    first = getinfo();
//    printf("First=temp? addr: %p\n", &first);
//}
//
//int makeinfo(int info) {
//    info = 9;
//    return info;
//}
//
//void C14Example_2() {
//    int a = 5, b = 4;
//    makeinfo(a);
//    printf("a=%d\n", a);
//    b = makeinfo(a);
//    printf("b=%d\n", b);
//    a = makeinfo(a);
//    printf("a1=%d\n", a);
//}
//
////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//#define LEN 30
//
//struct names {
//    char first[LEN];
//    char last[LEN];
//};
//
//struct guy {
//    struct names handle;
//    char favfood[LEN];
//    char job[LEN];
//    float income;
//};
//
//int C14P387(void) //使用指向结构的指针
//{
//    struct guy fellow[2] =
//            {
//                    {{"Ewen",   "Villard"},
//                            "grilled salmon",
//                            "personality coach",
//                            58112.00
//                    },
//                    {{"Rodney", "swillbelly"},
//                            "tripe",
//                            "tabloid editor",
//                            232400.00
//                    }
//            };
//
//    struct guy *him;
//    printf("address #1:%p #2:%p\n", &fellow[0], &fellow[1]);
//    him = &fellow[0];
//    printf("pointer #1:%p #2:%p\n", him, him + 1);
//    printf("him->income is $%.2f: (*him).income is $%.2f\n", him->income, (*him).income);
//
//    him++;
//    printf("him->facfood is %s: him->handle.last is %s\n", him->favfood, him->handle.last);
//    return 0;
//
//}
////~~~~~~~~~~~~~~~~~~~~~~~~~~
//
////him=&barney
////barney.income == (*him).income == him->income
////向函数传递结构信息
//
//#define FUNDLEN 50
//
////struct funds{
////    char bank[FUNDLEN];
////    double bankfund;
////    char save[FUNDLEN];
////    double savefund;
////};
////double sum1(double,double);
////int C14P389(void) //把结构成员作为参数传递
////{
////    struct funds stan=
////    {
////        "Garlic-Melon Bank",
////        3024.72,
////        "Lucky's Savings and Loan",
////        9237.11
////    };
////    printf("Stan has a total of $%.2f.\n",sum1(stan.bankfund,stan.savefund));
////    return 0;
////}
////
////double sum1(double x,double y)
////{
////    return x+y;
////}
//
////>>>>>>>>>>>>>>>>>>
//
////struct funds{
////    char bank[FUNDLEN];
////    double bankfund;
////    char save[FUNDLEN];
////    double savefund;
////};
////double sum2(const struct funds *);
////int C14P390(void) //传递指向结构的指针
////{
////    struct funds stan=
////    {
////        "Garlic-Melon Bank",
////        3024.72,
////        "Lucky's Savings and Loan",
////        9237.11
////    };
////    printf("Stan has a total of $%.2f.\n",sum2(&stan));
////    return 0;
////}
////double sum2(const struct funds * money)
////{
////    return (money->bankfund + money->savefund);
////}
//
////>>>>>>>>>>>>>>>>>
//
//struct funds {
//    char bank[FUNDLEN];
//    double bankfund;
//    char save[FUNDLEN];
//    double savefund;
//};
//
//double sum3(struct funds moolah); //参数是一个结构
//int C14P391(void) {
//    struct funds stan =
//            {
//                    "Garlic-Melon Bank",
//                    3024.72,
//                    "Lucky's Savings and Loan",
//                    9237.11
//            };
//    printf("Stan has a total of $%.2f.\n", sum3(stan));
//    return 0;
//}
//
//double sum3(struct funds moolah) {
//    return (moolah.bankfund + moolah.savefund);
//}
//
////~~~~~~~~~~~~~~~~~~~~~~
//
////struct namect{
////    char fname[20];
////    char lname[20];
////    int letters;
////};
////void getinfo1(struct namect *);
////void makeinfo1(struct namect *);
////void showinfo1(const struct namect *);
////
////int C14P392(void)
////{
////    struct namect person;
////    getinfo1(&person);
////    makeinfo1(&person);
////    showinfo1(&person);
////    return 0;
////}
////void getinfo1(struct namect * pst)
////{
////    printf("Please enter your first name.\n");
////    gets(pst->fname);
////    printf("Please enter your last name.\n");
////    gets(pst->lname);
////}
////void makeinfo1(struct namect * pst)
////{
////    //warning:隐式转换失去整数精度????????
////    pst->letters = strlen(pst->fname) + strlen(pst->lname);
////}
////void showinfo1(const struct namect * pst)
////{
////    printf("%s %s your name contains %d letters.\n",pst->fname,pst->lname,pst->letters);
////}
//
////>>>>>>>>>>>>>>>>>>>>>>>>>
//
//struct namect {
//    char fname[20];
//    char lname[20];
//    long letters;
//};
//
//struct namect getinfo2(void);
//
//struct namect makeinfo2(struct namect);
//
//void showinfo2(struct namect);
//
//int C14P393() {
//    struct namect person;
//    person = getinfo2();
//    person = makeinfo2(person);
//    showinfo2(person);
//    return 0;
//}
//
//struct namect getinfo2(void) {
//    struct namect temp;
//    printf("Please enter your first name.\n");
//    gets(temp.fname);
//    printf("Please enter your last name.\n");
//    gets(temp.lname);
//    return temp;
//}
//
//struct namect makeinfo2(struct namect info) {
//    info.letters = strlen(info.fname) + strlen(info.lname);
//    return info;
//}
//
//void showinfo2(struct namect info) {
//    printf("%s %s your name contains %ld letters.\n", info.fname, info.lname, info.letters);
//
//}
//
////~~~~~~~~~~~~~~~~~~~~
////struct vector{
////    float x;
////    float y;
////};
////
////struct vector sum_vect(struct vector ,struct vector);
////void C14P394()
////{
////
////    struct vector ans, a={1,3}, b={2,4};
////    ans=sum_vect(a, b);
////    printf("%f %f",ans.x,ans.y);
////}
////struct vector sum_vect(struct vector V1,struct vector V2)
////{
////    struct vector temp;
////    temp.x= V1.x + V2.x;
////    temp.y= V1.y + V2.y;
////    return temp;
////}
//
////>>>>>>>>>>>>
//
//struct vector {
//    float x;
//    float y;
//};
//
//void sum_vect(struct vector *, struct vector *, struct vector *);
//
//void C14P395() {
//    struct vector ans, a = {1, 3}, b = {2, 4};
//    sum_vect(&a, &b, &ans);
//    printf("%f %f", ans.x, ans.y);
//
//}
//
//void sum_vect(struct vector *V1, struct vector *V2, struct vector *ans) {
//
//    ans->x = V1->x + V2->x;
//    ans->y = V1->y + V2->y;
////      printf("%f %f",ans->x,ans->y);
//}
//
//struct nameString {
//    char *lastName;
//    char firstName[40];
//
//};
//
//void MemoryAllocate(struct nameString *name) {
////    char str[50];
////    name->lastName = str;
//
//
//    name->lastName = (char *) malloc(50);
//
//    //malloc: memory allocate
//    char *p;
//    char c = 'A';
//    char str[50] = "ewewe";
//    p = &c;
//    p = str;
//}
//
//struct name {
//    char *fname;
//    char *lname;
//    int letter;
//};
//
//void C14P396(struct name *pst) {
//    char temp[81];
//    printf("Please enter your first name.\n");
//    gets(temp);
//    pst->fname = (char *) malloc(strlen(temp) + 1);
//    strcpy(pst->fname, temp);
//    printf("Please enter your last name.\n");
//    gets(temp);
//    pst->lname = (char *) malloc(strlen(temp) + 1);
//    strcpy(pst->lname, temp);
//
//}
//
//struct myname {
//    char *fname;
//    char *lname;
//    long letters;
//};
//
//void getinfo3(struct myname *);
//
//void makeinfo3(struct myname *);
//
//void showinfo3(struct myname *);
//
//void cleanup(struct myname *);
//
//int C16P397() {
//    struct myname person;
//    getinfo3(&person);
//    makeinfo3(&person);
//    showinfo3(&person);
//    cleanup(&person);
//    return 0;
//}
//
//void getinfo3(struct myname *pst) {
//    char temp[81];
//    printf("Please enter your first name.\n");
//    gets(temp);
//    pst->fname = (char *) malloc(strlen(temp) + 1);
//    strcpy(pst->fname, temp);
//    printf("Please enter your last name.\n");
//    gets(temp);
//    pst->lname = (char *) malloc(strlen(temp) + 1);
//    strcpy(pst->lname, temp);
//
//}
//
//void makeinfo3(struct myname *pst) {
//    pst->letters = strlen(pst->fname) + strlen(pst->lname);
//}
//
//void showinfo3(struct myname *pst) {
//    printf("%s %s,your name contains %ld letters.\n", pst->fname, pst->lname, pst->letters);
//}
//
//void cleanup(struct myname *pst) {
//    free(pst->fname);
//    free(pst->lname);
//}
//
//struct flex {
//    int count;
//    double average;
//    double scores[];
//};
//
//void showflex(const struct flex *p);
//
//int C16P399() {
//    struct flex *pf1, *pf2;
//    int n = 5;
//    int i;
//    int tot = 0;
//    pf1 = malloc(sizeof(struct flex) + n * sizeof(double));
//    pf1->count = n;
//    for (i = 0; i < n; i++) {
//        pf1->scores[i] = 20.0 - i;
//        tot += pf1->scores[i];
//    }
//    pf1->average = tot / n;
//    showflex(pf1);
//
//    n = 9;
//    tot = 0;
//    pf2 = malloc(sizeof(struct flex) + n * sizeof(double));
//    pf2->count = n;
//    for (i = 0; i < n; i++) {
//        pf2->scores[i] = 20.0 - i / 2.0;
//        tot += pf2->scores[i];
//    }
//    pf2->average = tot / n;
//    showflex(pf2);
//
//    free(pf1);
//    free(pf2);
//
//    return 0;
//}
//
//void showflex(const struct flex *p) {
//    int i;
//    printf("scores:");
//    for (i = 0; i < p->count; i++)
//        printf("%g ", p->scores[i]);
//    printf("\naverage:%g\n", p->average);
//}
//
////#define FUNDLEN 50
//#define N 2
//
//struct funds1 {
//    char bank[FUNDLEN];
//    double bankfund;
//    char save[FUNDLEN];
//    double savefund;
//};
//
//double sum1(const struct funds1 money[], int n);
//
//int C16P400() {
//    struct funds1 jones[N] = {
////        [0]=
//            {
//                    "Garlic-Melon Bank",
//                    3024.72,
//                    "Lucky's Savings and Loan",
//                    9237.11
//            },
////        [1]=
//            {
//                    "Honest Jack's Bank",
//                    3534.28,
//                    "Party Time Savings",
//                    3203.89
//            }
//    };
//    printf("The Joneses have a total of $%.2f.\n", sum1(jones, N));
//    return 0;
//}
//
//double sum1(const struct funds1 money[], int n) {
//    double total;
//    int i;
//    for (i = 0, total = 0; i < n; i++)
//        total += money[i].bankfund + money[i].savefund;
//    return (total);
//
//}
//
//void BubbleSort() {
//    int arr[10] = {2, 5, 3, 8, 12, 54, 87, 1, 33, 9};
//    for (int i = 0; i < 9; i++)
//        for (int j = i + 1; j < 10; j++) {
//            int temp = 0;
//            if (arr[i] > arr[j]) {
//                temp = arr[i];
//                arr[i] = arr[j];
//                arr[j] = temp;
//            }
//        }
//}
//
////enum spectrum {red, orange, yellow, green, blue, violet};
//const char *colors[] = {"red", "orange", "yellow", "gereen", "blue", "violet"};
//
//int C16P409() {
//    char choice[LEN];
////    enum spectrum color;
//    bool color_is_found = false;
//
//    puts("Enter a color (empty line to quit):");
//    while (gets(choice) != NULL && choice[0] != '\0') {
////        for(color=red;color<=violet;color++)
//        for (int i = 0; i < 6; i++) {
////            if(strcmp(choice, colors[color])==0)
//            if (strcmp(choice, colors[i]) == 0) {
//                color_is_found = true;
//                break;
//            }
//        }
//        if (color_is_found) {
//            if (strcmp(choice, "red") == 0)
//                printf("Roses are red.");
//            if (strcmp(choice, "orange") == 0)
//                puts("Poppies are orange.");
//            if (strcmp(choice, "yellow") == 0)
//                puts("Sunflowers are yellow.");
//
//        }
////            switch(color)
////        {
//
////        case red : puts("Roses are red.");
////            break;
////        case orange : puts("Poppies are orange.");
////            break;
////        case yellow : puts("Sunflowers are yellow.");
////            break;
////        case green : puts("Grass is green.");
////            break;
////        case blue : puts("Bluebells are blue.");
////            break;
////        case violet : puts("violets are violet.");
////            break;
//
////        }
//        else
//            printf("I don't know about the color %s.\n", choice);
//        color_is_found = false;
//        puts("Next color,please(empty line to quit):");
//    }
//    puts("Goodbye!");
//    return 0;
//}
//
//char showmenu(void);
//
//void eatline(void);
//
//void show(void(*fp)(char *), char *str);
//
//void ToUpper(char *);
//
//void ToLower(char *);
//
//void Transpose(char *);
//
//void Dummy(char *);
//
//int C16P415(void) {
//    char line[81];
//    char copy[81];
//    char choice;
//    void (*pfun)(char *);
//    puts("Enter a string(empty line to quit):");
//    while (gets(line) != NULL && line[0] != '\0') {
//        while ((choice = showmenu()) != 'n') {
//            pfun = 0;
//            switch (choice) {
//                case 'u':
//                    pfun = ToUpper;
//                    break;
//                case 'l':
//                    pfun = ToLower;
//                    break;
//                case 't':
//                    pfun = Transpose;
//                    break;
//                case 'o':
//                    pfun = Dummy;
//                    break;
//            }
//            strcpy(copy, line);
////            show(pfun, copy);
//            if (pfun != 0) {
//                (*pfun)(copy);
//                puts(copy);
//            }
//
//        }
//
//        puts("Enter a string(empty line to quit):");
//    }
//    puts("Bye!");
//    return 0;
//}
//
//char showmenu(void) {
//    char ans;
//    puts("Enter menu choice:");
//    puts("u)uppercase l)lowercase");
//    puts("t)transposed case o)original case");
//    puts("n)next string");
//    ans = getchar();
//    ans = tolower(ans);
//    eatline();
//    while (strchr("ulton", ans) == NULL) {
//        puts("Please enter a u, l, t, o, or n:");
//        ans = tolower(getchar());
//        eatline();
//    }
//    return ans;
//}
//
//void eatline(void) {
//    while (getchar() != '\n')
//        continue;
//}
//
//void ToUpper(char *str) {
//    while (*str) {
//        *str = toupper(*str);
//        str++;
//    }
//}
//
//void ToLower(char *str) {
//    while (*str) {
//        *str = tolower(*str);
//        str++;
//    }
//}
//
//void Transpose(char *str) {
//    while (*str) {
//        if (islower(*str))
//            *str = toupper(*str);
//        else if (isupper(*str))
//            *str = tolower(*str);
//        str++;
//
//    }
//}
//
//void Dummy(char *str) {
//
//}
//
//void show(void (*fp)(char *), char *str) {
//    (*fp)(str);
//    puts(str);
//}
//
//void ABC() {
//    char a, b;
//    a = getchar();
//    printf("a=%c\n", a);
//
//    b = getchar();
//    printf("b=%c", b);
//
//
//}
//
////C16P419_3
//struct month {
//    char name[10];
//    char abbre[3];
//    int days;
//    int monumbe;
//
//};
//
//void C16P419_3(void) {
//    struct month months[12] = {
//            {"January",   "Jan", 31, 1},
//            {"Fabruary",  "Fab", 28, 2},
//            {"March",     "Mar", 31, 3},
//            {"April",     "Apr", 30, 4},
//            {"May",       "May", 31, 5},
//            {"June",      "Jun", 30, 6},
//            {"July",      "Jul", 31, 7},
//            {"August",    "Aug", 31, 8},
//            {"September", "Sep", 30, 9},
//            {"October",   "Oct", 31, 10},
//            {"November",  "Nov", 30, 11},
//            {"December",  "Dec", 31, 12}
//    };
//
//    int monthnum, index, total;
//    printf("Enter the month:");
//    //scanf("%d",&monthnum);
//
//    do {
//        scanf("%d", &monthnum);
//
//        if (monthnum < 1 || monthnum > 12)
//            printf("Erorr!Please input again");
//        else {
//            for (index = 0, total = 0; index < monthnum; index++)
//                total += months[index].days;
//            printf("total=%d\n", total);
//        }
//    } while (monthnum);
//}
//
//void toupper2(char *);
//
//char toupper1(char);
//
//void test1013() {
////    char abc = 'a';
//    char abc;
//
////    toupper2(&abc);
//
////    abc = toupper1(abc);
//    printf("Enter a letter:");
//    scanf("%c", &abc);
//    abc = toupper1(abc);
//    printf("%c", abc);
//}
//
//void toupper2(char *ch) {
//    switch (*ch) {
//        case 'a':
//            *ch = 'A';
//            break;
//        case 'b':
//            *ch = 'B';
//            break;
//        case 99:
//            *ch = 67;
//            break;
//
//        default:
//            break;
//    }
//}
//
//char toupper1(char ch) {
////    ch = 'A';
////    return ch;
//
////    switch (ch)
////    {
////        case 'a':
////            ch = 'a'-32;
////            break;
////        case 'b':
////            ch = 'b'-32;
////            break;
////
////        default:
////            break;
////    }
//    if (ch > 96 && ch < 123) {
//        ch -= 32;
//        return ch;
//    } else if (ch > 64 && ch < 91) {
//        ch += 32;
//        return ch;
//    }
//
//    return ch;
//}
//
//void tolower1(char *ch);
//
//void test1014() {
//    char abc;
//    printf("Enter the letter:");
//    scanf("%c", &abc);
//    tolower1(&abc);
//    printf("%c", abc);
//}
//
//void tolower1(char *ch) {
//    if (*ch > 64 && *ch < 91)
//        *ch += 32;
//}
//
//char tolower2(char ch);
//
//void test1015() {
//    char abc;
//    printf("Enter the letter:");
//    scanf("%c", &abc);
//    abc = tolower(abc);
//    printf("%c", abc);
//}
//
//char tolower2(char ch) {
//    if (ch > 64 && ch < 91)
//        ch += 32;
//    return ch;
//}
//
//typedef struct {
//    float foclen;
//    float fstop;
//    char brand[30];
//
//} LENS;
//
//void C17P419_6() {
//    LENS bigeye[10];
////    bigeye[2].foclen=500;
////    bigeye[2].fstop=2.0;
////    char a[30]="Remarkatar";
//    strcpy(bigeye[2].brand, "Remarkatar");
//
////    LENS bigeye[2]={500, 2.0, "Remarkatar"};
//    LENS bigeye1[10] = {[2]={500, 2.0, "Remarkatar"}};
//}
//
//struct name2 {
//    char first[20];
//    char last[20];
//};
//struct bem {
//    int limbs;
//    struct name2 title;
//    char type[30];
//};
//
//void C16P420_7() {
//    struct bem *pb;
//    struct bem deb = {
//            6,
//            {"Berbnazel", "Gwolkapwolk"},
//            "Arcturan"
//    };
//
//    pb = &deb;
//
//    printf("%d\n", deb.limbs);
//    printf("%s\n", pb->type);
//    printf("%s\n", pb->type + 2);
//}
//
////#include <starfolk.h>
//void showp(struct bem *p) {
//    struct bem deb = {
//            6,
//            {"Berbnazel", "Gwolkapwolk"},
//            "Arcturan"
//    };
//
//    p = &deb;
//    printf("%s %s is a %d-limbed %s", p->title.first, p->title.last, p->limbs, p->type);
//}
//
//struct car {
//    char name[10];
//    float horsepawer;
//    float epampg;
//    float wheelbase;
//    int year;
//};
//
//struct gas {
//    float distance;
//    float gals;
//    float mpg;
//};
//
//struct gas mpgs(struct gas trip) {
//    if (trip.gals > 0)
//        trip.mpg = trip.distance / trip.gals;
//    else
//        trip.mpg = -1;
//
//    return trip;
//}
//
//void set_mpgs(struct gas *trip) {
//    if (trip->gals > 0)
//        trip->mpg = trip->distance + trip->gals;
//    else
//        trip->mpg = -1;
//}
//
//void C16P421_10b() {
//    struct gas Day = {143.0, 14.8};
//    Day = mpgs(Day);
//    struct gas Day1 = {583, 17, 6};
//    set_mpgs(&Day1);
//
//}
//
//enum choices {
//    no, yes, maybe
//};
//
//char (*p)(char *, char);
////C16P421_12
////double A (double, double);
////double B (double, double);
////double C (double, double);
////double D(double, double);
////double (*pst[4])(double, double)={A, B, C, D};
//
//
//
//
//int days(char *p);
//
//struct month1 {
//    char name[10];
//    char abbre[3];
//    int days;
//    int monumbe;
//};
//
//struct month1 months[12] = {
//        {"January",   "Jan", 31, 1},
//        {"Fabruary",  "Fab", 28, 2},
//        {"March",     "Mar", 31, 3},
//        {"April",     "Apr", 30, 4},
//        {"May",       "May", 31, 5},
//        {"June",      "Jun", 30, 6},
//        {"July",      "Jul", 31, 7},
//        {"August",    "Aug", 31, 8},
//        {"September", "Sep", 30, 9},
//        {"October",   "Oct", 31, 10},
//        {"November",  "Nov", 30, 11},
//        {"December",  "Dec", 31, 12}
//};
//
//void C16P421_1() {
//    char input[10];
//    int daytotal;
//
//    printf("Enter the name of a month:");
//    while (gets(input) != NULL && input[0] != '\0') {
//        daytotal = days(input);
//        if (daytotal > 0)
//            printf("There are %d days through %s.\n", daytotal, input);
//        else
//            printf("%s is not valid input.\n", input);
//        printf("Next month(empty line to quit):");
//    }
//    printf("Bye");
//}
//
//int days(char *p) {
//    int i = 0, total = 0;
//    while (p[i] != '\0') {
//        if (i == 0)
//            p[i] = toupper(p[i]);
//        else
//            p[i] = tolower(p[i]);
//        i++;
//    }
//    for (i = 0; i < 12; i++) {
//        total += months[i].days;
//        if (p == months[i].name) //if(strcmp(p, months[i].name)==0)
//            return total;
//    }
//    return -1;
//
//}
//
//int leapyear(int);
//
//int days1(int, int, int);
//
//void C16P421_2() {
//    int day = 1, mon = 12, year = 1, daytotal;
//    printf("Enter the number of day,month,year:");
//    while (scanf("%d%d%d", &day, &mon, &year) == 3) {
//        daytotal = days1(day, mon, year);
//        if (daytotal > 0)
//            printf("There are %d day through day %d,month %d,year %d.\n", daytotal, day, mon, year);
//        else
//            printf("day %d,month %d,year %d is not valid input.\n", day, mon, year);
//        printf("Next month(input q to quit):");
//    }
//    printf("Bye");
//}
//
//int days1(int day, int mon, int year) {
//    int i, total;
//    if (leapyear(year))
//        months[1].days = 29;
//    else
//        months[1].days = 28;
//    if (mon < 1 || mon > 12 || day < 1 || day > months[mon - 1].days)
//        return -1;
//    else {
//        for (i = 0, total = 0; i < mon - 1; i++)
//            total += months[i].days;
//        return (total + day);
//    }
//}
//
//int leapyear(int year) {
//    if (year % 100 == 0 && year % 400 == 0)
//        return 1;
//    else if (year % 100 != 0 && year % 4 == 0)
//        return 1;
//    else
//        return 0;
//
//}
//
//void test() {
//    char ch[10];
////    gets(ch);
////    printf("%s",ch);
//
//    if (gets(ch) != NULL)
//        printf("%s", ch);
//    else
////        printf("%d",ch);
//        printf("lalalla");
//}
//
//void test1() {
////    char ch[4];
//    int i;
////
////    char*p=NULL;
////    if(p==NULL)
////        printf("NULL p");
////
////    for(i=0;i<4;i++)
////        printf("%d",ch[i]);
////
////    printf("\n");
////    long len = strlen(gets(ch));
////    for(i=0;i<len;i++)
////        printf("%d ",ch[i]);
////    printf("Length: %ld",len);
//
//    i = getchar();
//    if (i != '\n')
//        printf("%c\n", i);
//    else
//        printf("%d", i);
//    printf("%d\n", getchar());
//    printf("%d\n", getchar());
//    printf("%d\n", getchar());
//
//
////    i=getchar();
////    printf("%d",i);
//
//
//
//
//}
//
//void test2() {
//    int ch[10];
//    printf("Enter 10 numbers:");
//    for (int index = 0; index < 10; index++)
//        scanf("%d", &ch[index]);
//    int temp;
//    for (int i = 0; i < 9; i++) {
//        for (int j = i + 1; j < 9; j++) {
//            if (ch[i] > ch[j]) {
//                temp = ch[i];
//                ch[i] = ch[j];
//                ch[j] = temp;
//            }
//        }
//    }
//    for (int index = 0; index < 10; index++)
//        printf("%d", ch[index]);
//}
//
//#define MAXTITL 40
//#define MAXAUTH 40
//#define MAXBKS 100
//
//
//struct mybook {
//    char title[MAXTITL];
//    char author[MAXAUTH];
//    float value;
//};
//
//void sort_title(struct mybook *, int);
//
//void sort_value(struct mybook *, int);
//
//int C16P383() {
//    struct mybook library[MAXBKS];
//    int count = 0;
//    int index;
//
//    printf("Enter the book title:\n");
//    printf("Please [enter] at the start of a line to stop.\n");
//    while (count < MAXBKS && gets(library[count].title) != NULL && library[count].title[0] != '\0') {
//        printf("Now enter the author:\n");
//        gets(library[count].author);
//        printf("Now enter the value:\n");
//        scanf("%f", &library[count++].value); //会有回车符存在缓存中
//        while (getchar() != '\n')
//            continue;
//        if (count < MAXBKS)
//            printf("Enter the next title.\n");
//    }
//    if (count > 0) //如果最开始printf就出错，那么就不会走这步了
//    {
//        printf("Here is the list of your books:\n");
//        for (index = 0; index < count; index++)
//            printf("%s by %s:$%.2f\n", library[index].title, library[index].author, library[index].value);
//
//        printf("\nHere is the list of your books by title:\n");
//        sort_title(&library[0], count);
//        for (index = 0; index < count; index++)
//            printf("%s by %s:$%.2f\n", library[index].title, library[index].author, library[index].value);
//
//        printf("\nHere is the list of your books by value:\n");
//        sort_value(&library[0], count);
//        for (index = 0; index < count; index++)
//            printf("%s by %s:$%.2f\n", library[index].title, library[index].author, library[index].value);
//    }
//    return 0;
//}
//
//void sort_title(struct mybook *p, int count) {
//    int i, j;
//    struct mybook temp;
//    for (i = 0; i < count; i++) {
//        for (j = i + 1; j < count; j++) {
//            if (p[i].title[0] > p[j].title[0]) {
//                temp = p[i];
//                p[i] = p[j];
//                p[j] = temp;
//            }
//        }
//    }
//}
//
//void sort_value(struct mybook *p, int count) {
//    int i, j;
//    struct mybook temp;
//    for (i = 0; i < count; i++) {
//        for (j = i + 1; j < count; j++) {
//            if (p[i].value > p[j].value) {
//                temp = p[i];
//                p[i] = p[j];
//                p[j] = temp;
//            }
//        }
//    }
//}
////~~~~~~~~~~~~~~~~~~
//
//
//struct namest {
//    char firstname[20];
//    char middlename[20];
//    char lastname[20];
//};
//
//struct persons {
//    int number;
//    struct namest name;
//};
//
//struct persons person[5] = {
//        {302039823, {"Dribble", "Mackede", "Flossie"}},
//        {345345345, {"gadenfs", "Kasdfas", "Pszajkh"}},
//        {302039823, {"Kazsdfj", "Aasdfaf", "Mjasdfh"}},
//        {302039823, {"Qaadsef", "",        "Yjsjdsh"}},
//        {302039823, {"Bsdsdfs", "Fsjdsdd", "Rshdsdf"}}
//};
//
//void display1(struct persons *);
//
//int C16P421() {
//    display1(person);
//    return 0;
//}
//
//void display1(struct persons *p) {
//    int i;
//
//    for (i = 0; i < 5; i++) {
//        if (p[i].name.middlename[0] != '\0')
//            printf("%s, %s %c. -%d\n", p[i].name.firstname, p[i].name.lastname, p[i].name.middlename[0], p[i].number);
//        else
//            printf("%s, %s -%d\n", p[i].name.firstname, p[i].name.lastname, p[i].number);
//    }
//}
//
////~~~~~~~~~~~
//struct namest1 {
//    char firstname[20];
//    char middlename[20];
//    char lastname[20];
//};
//
//struct persons1 {
//    int number;
//    struct namest1 name;
//};
//
//struct persons1 person1[5] = {
//        {302039823, {"Dribble", "Mackede", "Flossie"}},
//        {345345345, {"gadenfs", "Kasdfas", "Pszajkh"}},
//        {302039823, {"Kazsdfj", "Aasdfaf", "Mjasdfh"}},
//        {302039823, {"Qaadsef", "",        "Yjsjdsh"}},
//        {302039823, {"Bsdsdfs", "Fsjdsdd", "Rshdsdf"}}
//};
//
//void display2(struct persons1 person1[]);
//
//
//int C16P421_b() {
//    display2(person1);
//    return 0;
//}
//
//void display2(struct persons1 person1[5]) {
//    int i;
//
//    for (i = 0; i < 5; i++) {
//        if (person[i].name.middlename[0] != '\0')
//            printf("%s, %s %c. -%d\n", person1[i].name.firstname, person1[i].name.lastname,
//                   person1[i].name.middlename[0], person1[i].number);
//        else
//            printf("%s, %s -%d\n", person1[i].name.firstname, person1[i].name.lastname, person1[i].number);
//    }
//}
//
//struct name_a {
//    char firstname[20];
//    char lastname[20];
//};
//
//struct student {
//    struct name_a name;
//    float grade[3];
//    float average;
//};
//
//void set_score(struct student *);
//
//void calculate_average(struct student *);
//
//void print_each(struct student *);
//
//void print_class_average(struct student *);
//
//int C16P421_5() {
//    struct student students[4] = {
//            {{"Aasdda", "Basdjas"}, 0, 0, 0, 0},
//            {{"Csdfss", "Ddsfsdd"}, 0, 0, 0, 0},
//            {{"Edfdsf", "Fsfdsfd"}, 0, 0, 0, 0},
//            {{"Gfdsse", "Hservvd"}, 0, 0, 0, 0}
//    };
//    printf("select function :d,e,f,g\n");
//    printf("d:acquire scores for each student\n");
//    printf("e:calculate the average score value for each structure and assign it to the proper member\n");
//    printf("f:print the information in each tructure\n");
//    printf("g:print the class average for each of the numeric structure members\n");
//
//    while (1) {
//        switch (getchar()) {
//            case 'd':
//                set_score(students);
//                break;
//            case 'e':
//                calculate_average(students);
//                break;
//            case 'f':
//                print_each(students);
//                break;
//            case 'g':
//                print_class_average(students);
//                break;
//            case '\n':
//                break;
//            default :
//                puts("Bye");
//        }
//    }
//    return 0;
//}
//
//void set_score(struct student *p) {
//    int i, j;
//    for (i = 0; i < 4; i++) {
//        printf("Please input the 3 scores of %s %s:\n", p[i].name.firstname, p[i].name.lastname);
//        for (j = 0; j < 3; j++)
//            scanf("%f", &p[i].grade[j]);
//    }
//    printf("input finished!\n");
//}
//
//void calculate_average(struct student *p) {
//    int i, j;
//    float total;
//    for (i = 0; i < 4; i++) {
//        for (j = 0, total = 0; j < 3; j++) {
//            total += p[i].grade[j];
//            p[i].average = total / 3;
//        }
//    }
//    printf("calculate finished!\n");
//}
//
//void print_each(struct student *p) {
//    int i, j;
//    for (i = 0; i < 4; i++) {
//        printf("%s %s:\t", p[i].name.firstname, p[i].name.lastname);
//        for (j = 0; j < 3; j++)
//            printf("score%d:%f:\t", j + 1, p[i].grade[j]);
//        printf("average:%f\t", p[i].average);
//    }
//}
//
//void print_class_average(struct student *p) {
//    int i;
//    float total;
//    for (i = 0; i < 4; i++)
//        total += p[i].average;
//    printf("class average:%f\n", total / 4);
//}
//
////~~~~~~~~~~~~~~~
//#define MAX 12
//
//struct seat {
//    int number;
//    int assign;
//    char firstname[10];
//    char lastname[10];
//};
//
//struct seat seats[MAX] = {
//        {101, 0},
//        {102, 0},
//        {103, 1, "D", "wu"},
//        {104, 0},
//        {105, 0},
//        {106, 0},
//        {107, 1, "A", "tian"},
//        {108, 0},
//        {109, 0},
//        {110, 0},
//        {111, 1, "T", "ren"},
//        {112, 0}
//};
//
//
//void choose();
//
//void show_number_empty(struct seat *);
//
//void show_list_empty(struct seat *);
//
//void show_list(struct seat *);
//
//void assign_seat(struct seat *);
//
//void delete_seat(struct seat *);
//
//bool isExit = false;
//
//void C10P422_8() {
//    while (!isExit) {
//        choose();
//    }
//}
//
//void choose() {
//
////    char command[10];
//    printf("To choose a function, enter its letter label\n"); //要选择功能，请输入其字母标签
//    printf("a)Show number of empty seats \n"); //显示空座位数
//    printf("b)Show list of empty seats \n"); //显示空座位列表
//    printf("c)Show alphabetical list of seats \n"); //按字母顺序排列座位列表
//    printf("d)Assign a customer to a seat assignment \n"); //将客户分配给座位分配
//    printf("e)Delete a seat assignment \n"); //删除座位分配  ???删除117号之后再执行a,数据不会更新
//    printf("f)Quit \n");
//    switch (getchar()) {
//        case 'a':
//            show_number_empty(seats);
//            break;
//        case 'b':
//            show_list_empty(seats);
//            break;
//        case 'c':
//            show_list(seats);
//            break;
//        case 'd':
//            assign_seat(seats);
//            break;
//        case 'e':
//            delete_seat(seats);
//            break;
//        case 'f':
//            puts("Quit");
//            isExit = true;
//            return; //想退出程序，可以这么写吗？？？？？？？
//        default :
//            break;
//
//    }
//    getchar();
//}
//
//void show_number_empty(struct seat *p) {
//    int i, n;
//    for (i = 0, n = 0; i < MAX; i++) {
//        if (p[i].assign == 0)
//            n++;
//    }
//    printf("There are %d empty seats\n", n);
//}
//
//void show_list_empty(struct seat *p) {
//    int i;
//    for (i = 0; i < MAX; i++) {
//        if (p[i].assign == 0)
//            printf("number%d ", p[i].number);
//    }
//    printf("\n");
//}
//
//void show_list(struct seat *p) {
//    int i, j, index;
//    struct seat temp;
//    for (i = 0; i < MAX; i++) {
//        for (j = i + 1; j < MAX; j++) {
//            if (p[i].firstname[0] > p[j].firstname[0]) {
//                temp = p[i];
//                p[i] = p[j];
//                p[j] = temp;
//            }
//        }
//    }
//    for (index = 0; index < MAX; index++)
//        printf("%d %d %s %s\n", p[index].number, p[index].assign, p[index].firstname, p[index].lastname);
//}
//
//void assign_seat(struct seat *p) {
//    int number, i;
//    printf("Input the seat number:");
//    scanf("%d", &number);
//    getchar();
//    for (i = 0; i < MAX; i++) {
//        if (p[i].number == number) {
//            if (p[i].assign == 1)
//                printf("No.%d seat is already assigned!\n", p[i].number);
//            else {
//                printf("Input firstname of the holder:");
//                scanf("%s", p[i].firstname);
//                getchar();
//                printf("Input lastname of the holder:");
//                scanf("%s", p[i].lastname);
//                getchar();
//                p[i].assign = 1;
//                printf("assign No.%d seat successfully!\n", p[i].number);
//            }
//        }
//    }
////    printf("%d is a invalid seat number!\n",number);
//}
//
//void delete_seat(struct seat *p) {
//    int number, i;
//    bool isFound = false;
//    printf("Input the seat number:");
//    scanf("%d", &number);
//    getchar();
//    for (i = 0; i < MAX; i++) {
//        if (p[i].number == number) {
//            if (p[i].assign == 0) {
//                printf("No.%d seat is already empty!\n", number);
//            } else {
//                p[i].assign = 0;
//                printf("Delete No.%d seat successfully!\n", number);
//
//            }
//            isFound = true;
//            break;
//        }
//    }
//
//    if (!isFound)
//        printf("%d is a invalid seat number\n", number);
//
//}
//
//float Test_P422(float x, float y) {
//    return 0;
//}
//
//float Test_P422_2(float x, float y) {
//    return 0;
//}
//
//void C16P422_10_a() {
//    float (*p)(float, float); //指向函数的指针P
//    p = Test_P422; //指针P指向Test_P422函数
//    (*p)(1.2, 2.3);
//
//    float *b[10];  // 含有10个元素的指针数组
//    int a[10] = {1, 2, 3}; //数组初始化
//
//    float (*arr[10])(float, float) = {Test_P422, Test_P422_2}; //含有10个元素的指针数组指向函数
//    (*arr[0])(1.2, 1.5);
//    (*arr[1])(99.2, 4.4);
//
//}
//
//#define F 4
//
//float add(float x, float y);
//
//float subtruct(float x, float y);
//
//float multiply(float x, float y);
//
//float divide(float x, float y);
//
//void eatline1();
//
//
//void C16P422_10() {
//    float (*p[F])(float x, float y) = {add, subtruct, multiply, divide};
//    while (1) {
//        float x, y;
//        int i = 0;
//        printf("Select function:\n");
//        printf("1 :x+y\n");
//        printf("2 :x-y\n");
//        printf("3 :x*y\n");
//        printf("4 :x/y\n");
//        printf("q :Quit\n");
////        scanf("%d",&i);
//        i = getchar() - '0';
//        eatline1();
////        getchar();
////        if(i=)
////            return 0;
//        if (i == 'q' - '0')
//            return;
////
//        if (i < 1 || i > 4)
//            printf("Please input again:\n");
//        else {
//            printf("Input x,y:");
//            scanf("%f %f", &x, &y);
//            getchar();
//            printf("result is:%f\n", (*p[i - 1])(x, y));
//        }
//    }
//}
//
//float add(float x, float y) {
//    return x + y;
//}
//
//float subtruct(float x, float y) {
//    return x - y;
//}
//
//float multiply(float x, float y) {
//    return x * y;
//}
//
//float divide(float x, float y) {
//    return x / y;
//}
//
//
//void eatline1() {
//    while (getchar() != '\n')
//        continue;
//}
//
//void to_binary(unsigned long n);
//
//int C9P228() {
//    unsigned long number;
//    printf("Enter an integer(q to quit):\n");
//    while (scanf("%lu", &number) == 1) {
//        printf("Binary equivalent:");
//        to_binary(number);
//        putchar('\n');
//        printf("Enter an integer (q to quit):\n");
//    }
//    printf("Done.\n");
//    return 0;
//}
//
//void to_binary(unsigned long n) {
//    int r;
//    r = n % 2;
//    if (n >= 2)
//        to_binary(n / 2);
//    putchar('0' + r);
//    return;
//}
//
//void test_10(unsigned long);
//
//void test_9() {
//    unsigned long n;
//    scanf("%lu", &n);
//
//    test_10(n);
//}
//
//void test_10(unsigned long n) {
//
//
//    if (n >= 2)
//        test_10(n / 2);
//    printf("%lu\n", n);
//}
//
//char *itobs(int, char *);
//
//void show_bstr(const char *);
//
//int invert_end(int num, int bits);
//
//int C16P430() {
//    char bin_str[8 * sizeof(int) + 1];
//    int number;
//    puts("Enter integers and see them in binary.");
//    puts("Non-numeric input terminates program:");
//    while (scanf("%d", &number) == 1) {
//        itobs(number, bin_str);
//        printf("%d is\n", number);
//        show_bstr(bin_str);
//        putchar('\n');
//        number = invert_end(number, 4);
//        printf("Inverting the last 4 bits gives\n");
//        show_bstr(itobs(number, bin_str));
//        putchar('\n');
//    }
//    puts("Bye!");
//    return 0;
//}
//
//char *itobs(int n, char *ps) {
//    int i;
//    static int size = 8 * sizeof(int);
//    for (i = size - 1; i >= 0; i--, n >>= 1)
//        ps[i] = (01 & n) + '0';
//    ps[size] = '\0';
//    return ps;
//}
//
//void show_bstr(const char *str) {
//    int i = 0;
//    while (str[i]) {
//        putchar(str[i]);
//        if (++i % 4 == 0)
//            putchar(' ');
//    }
//}
//
//int invert_end(int num, int bits) {
//    int mask = 0;  //0000 0000
//    int bitval = 1;  //0000 0001
//    while (bits-- > 0) {
//        mask |= bitval;  //mask=0000 0001 ->0000 0011->...->0000 1111
//        bitval <<= 1; //bitval=0000 0010->...->0000 1000
//    }
//    return num ^ mask;
//}
//
//void swap(int *, int *);
//
//void CRAND() {
//    int n;
//    int a[n];
////    int n1,n2,n3,n4,n5,n6,n7,n8;
//    printf("Please enter the number:");
//    scanf("%d", &n);
//    for (int i = 0; i < n - 1; i++)
//        a[i] = i;
//    for (int i = n - 1, m = 0; i >= 0; i--, m++) {
//        swap(&a[i], &a[rand() % (n - m)]);
//        printf("%d ", a[i]);
//    }
//    //    {
////
////        a[i-1]=rand()%(n-1);
////        printf("%d ",a[i-1]);
////    }
////
////    n2=(1+n1)%(n-1);
////    n3=(1+n2)%(n-1);
////    n4=(1+n3)%(n-1);
////    n5=(1+n4)%(n-1);
////    n6=(1+n5)%(n-1);
////    n7=(1+n6)%(n-1);
////    n8=(1+n7)%(n-1);
////    printf("n1=%d n2=%d n3=%d n4=%d n5=%d n6=%d n7=%d n8=%d",n1,n2,n3,n4,n5,n6,n7,n8);
//}
//
//void swap(int *x, int *y) {
//    int temp;
//    temp = *x;
//    *x = *y;
//    *y = temp;
//}
//
////~~~~~~~~~~~~~~~~~
//int bin_dec(char *p);
//
//char *pbin = "01001010";
////char * pbin = "00000001";
//
//void C6P444(void) {
//    printf("bin: %s is dec: %d\n", pbin, bin_dec(pbin));
//}
//
//int bin_dec(char *p) {
//    int dec = 0;
//    while (*p != '\0')
//        dec = (dec << 1) + *p++ - '0';
//    return dec;
//}
//
////~~~~~~~~~~~~~~~~~~~~
//
//int count_on(int n);
//
//void C16P445() {
//    int n;
//    printf("Input a number(q to quit):");
//    while (scanf("%d", &n) == 1) {
//        printf("%x----bit on number : %d\n", n, count_on(n));
//        printf("Input a number(q to quit):");
//    }
//    printf("quit\n");
//}
//
//int count_on(int n) {
//    int count = 0;
//    while (n / 2) {
//        count += n % 2;
//        n = n >> 1;
//    }
//    count += n % 2;
//    return count;
//}
//
////~~~~~~~~~~~~~~~~~~~~~~~~~
//int check_it(unsigned int number, unsigned int bit);
//
//int C16P445_4() {
//    unsigned int number, bit;
//    printf("Input a number and a bit position(q to quit):");
//    while (scanf("%d %d", &number, &bit) == 2) {
//        printf("bit %d of %d is %d\n", bit, number, check_it(number, bit));
//        printf("Input a number and a bit position(q to quit):");
//    }
//    printf("quit\n");
//    return 0;
//}
//
//int check_it(unsigned int number, unsigned int bit) {
//    return (number >> bit) & 01;
//}
////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//int rotate_2(unsigned int number, unsigned int bit);
//
//int C16P445_5() {
//    unsigned int number, bit;
//    printf("Input a hexadecimal number and rotated bits number(q to quit):");
//    while (scanf("%d %d", &number, &bit) == 2) {
//        printf("%ld rotate %d bit left:%ld\n", number, bit, rotate_2(number, bit));
//        printf("Input a number(q to quit)");
//    }
//    printf("quit\n");
//    return 0;
//}
//
//int rotate_2(unsigned int number, unsigned int bit) {
//    int left = number << bit;
//    int right = number >> 32 - bit;
//    return left + right;
//}
//
//
////~~~~~~~~~~
//int rotate_1(unsigned int number, unsigned int bit);
//
//int C16P445_5_2() {
//    unsigned int number, bit;
//    printf("Input a hexadecimal number and rotated bits number(q to quit):");
//    while (scanf("%d %d", &number, &bit) == 2) {
//        printf("%ld rotate %d bit left:%ld\n", number, bit, rotate_1(number, bit));
//        printf("Input a number(q to quit)");
//    }
//    printf("quit\n");
//    return 0;
//}
//
//int rotate_1(unsigned int number, unsigned int bit) {
//    unsigned int high = 8 * sizeof(unsigned int);
//    for (unsigned int i = 0; i < bit; i++) {
//        if (number & (1 << (high - 1)))
//            number = (number << 1) | 1;
//        else
//            number = number << 1;
//    }
//    return number;
//}
//
////~~~~~~~~~~~~~~~~~~~~~
//#define M 8*sizeof(int) + 1
//
//char *extend(char *destination, char *source);
//
//char *reverse(char *destination, char *source);
//
//char *and(char *destination, char *source1, char *source2);
//
//char *or(char *destination, char *source1, char *source2);
//
//char *exclusive_or(char *destination, char *source1, char *source2);
//
//int C16P444_2(int argc, char *argv[]) {
//
//    char x[M], y[M], z[M];
//
//    printf("    x = %s\n", extend(x, argv[1]));//拓展成M位宽
//    printf("    y = %s\n", extend(y, argv[2]));
//    printf("   ~x = %s\n", reverse(z, x));
//    printf("   ~y = %s\n", reverse(z, y));
//    printf("x & y = %s\n", and(z, x, y));
//    printf("x | y = %s\n", or(z, x, y));
//    printf("x ^ y = %s\n", exclusive_or(z, x, y));
//
//    return 0;
//}
//
//
//char *extend(char *destination, char *source)//将二进制字符串传给一个32位长的字符串
//{
//    unsigned int i;
//    for (i = 0; i < M - 1 - strlen(source); i++)
//        destination[i] = '0';
//    destination[i] = '\0';
//    strcat(destination, source); //连接字符串
//    return destination;
//}
//
//char *reverse(char *destination, char *source)//将二进制字符串 “取反”
//{
//    char *save = destination;
//    strcpy(destination, source);
//    while (*destination != '\0') {
//        if (*destination == '0')
//            *destination = '1';
//        else
//            *destination = '0';
//        destination++;
//    }
//    return save;
//}
//
//
//char *and(char *destination, char *source1, char *source2)//将两个二进制字符串 求与
//{
//    char *save = destination;
//    while (*source1 != '\0') {
//        if (*source1 == '1' && *source2 == '1')
//            *destination = '1';
//        else
//            *destination = '0';
//        source1++;
//        source2++;
//        destination++;
//    }
//    return save;
//}
//
//char *or(char *destination, char *source1, char *source2)//将两个二进制字符串 求或
//{
//    char *save = destination;
//    while (*source1 != '\0') {
//        if (*source1 == '1' || *source2 == '1')
//            *destination = '1';
//        else
//            *destination = '0';
//        source1++;
//        source2++;
//        destination++;
//    }
//    return save;
//}
//
//char *exclusive_or(char *destination, char *source1, char *source2)//将两个二进制字符串 求异或
//{
//    char *save = destination;
//    while (*source1 != '\0') {
//        if (*source1 != *source2)
//            *destination = '1';
//        else
//            *destination = '0';
//        source1++;
//        source2++;
//        destination++;
//    }
//    return save;
//}
//
////～～～～～～～
//void P31_6() {
//    for (int i = 3; i > 0; i--) {
//        for (int j = 1; j <= i; j++)
//            printf("smile!");
//        printf("\n");
//    }
//}
//
////～～～～～～
//void P114_2() {
//    int number;
//    printf("Please input a integer number(q to quit):");
//
//    while (scanf("%d", &number) == 1) {
//        for (int i = 0; i <= 10; i++)
//            printf("%d ", number + i);
//        printf("\nPlease input a integer number(q to quit):");
//    }
//    printf("\nBye");
//}
//
////~~~~~~~~~~
//void P114_3() {
//    int days;
//    printf("Please input the days:");
//    while (scanf("%d", &days) == 1 && days > 0) {
//        printf("%d days is %d week and %d days", days, days / 7, days % 7);
//        printf("\nPlease input the days(q to quit):");
//    }
//}
//
////~~~~~~~~~
//void P149_1() {
//    char ABC[26];
//    for (int i = 0; i < 26; i++) {
//        ABC[i] = 'a' + i;
//        printf("%c", ABC[i]);
//    }
//}
//
////~~~~~~~
//void P149_2() {
//    for (int i = 1; i <= 5; i++) {
//        for (int j = 1; j <= i; j++)
//            printf("$");
//        printf("\n");
//    }
//}
//
////~~~~~~~~
//void P149_3() {
//    for (int i = 0; i <= 5; i++) {
//        for (int j = 0; j <= i; j++)
//            printf("%c", 'F' - j);
//        printf("\n");
//    }
//}
//
////~~~~~~~
//void MyR(int *);
//
//void Random() {
//    int result[2] = {-1, -1};
//    int statistic[4] = {0, 0, 0, 0};
//    for (int i = 0; i < 100; i++) {
//        MyR(result);
//        for (int j = 0; j < 4; j++) {
//            if (result[0] == j)
//                statistic[j]++;
//            if (result[1] == j)
//                statistic[j]++;
//        }
//    }
//    printf("0=%f 1=%f 2=%f 3=%f", statistic[0] / 200.0, statistic[1] / 200.0, statistic[2] / 200.0,
//           statistic[3] / 200.0);
//}
//
//void MyR(int *array) {
//    array[0] = rand() % 4;
//    int n = rand() % 3;
//    if (n >= array[0])
//        n++;
//    array[1] = n;
//    printf("%d %d\n", array[0], array[1]);
//}
//
////~~~~~~~~~~~~~
//void P150_6() {
//    char str[20];
//    printf("Please enter a word:");
//    scanf("%s", &str);
//    for (int i = strlen(str) - 1; i >= 0; i--) {
//        printf("%c\n", str[i]);
//    }
//}
//
//void P150_10() {
//    int number[20];
//    printf("Please enter 8 numbers:");
//    for (int i = 0; i < 8; i++)
//        scanf("%d", &number[i]);
//    for (int i = 7; i >= 0; i--)
//        printf("%d ", number[i]);
//};
//
//void P186() {
//    int oddcount = 0, evencount = 0, oddsum = 0, evensum = 0;
//    int number;
//    printf("Please input a number:");
//
//    while (1) {
//        scanf("%d", &number);
//        if (number == 0)
//            break;
//        if (number % 2 == 0) {
//            evencount++;
//            evensum += number;
//        } else {
//            oddcount++;
//            oddsum += number;
//        }
//    }
//    printf("oddcount=%d oddaverage=%d evencount=%d oddaverage=%d", oddcount, oddsum / oddcount, evencount,
//           evensum / evencount);
//}
//
////$$$$$$$$$$$$$$$$$$$$$$$$$$
//
////Method_1
//
////void bubble_sort(int *, int);
////
////void bubble_sort(int arr[], int len) {
////    int i, j, temp;
////    for (i = 0; i < len - 1; i++) {
////        for (j = 0; j < len - 1 - i; j++) {
////            if (arr[j] > arr[j + 1]) {
////                temp = arr[j];
////                arr[j] = arr[j + 1];
////                arr[j + 1] = temp;
////            }
////        }
////    }
////}
//
////Method_2
////void bubble_sort2(int *, int);
//
////void bubble_sort2(int arr[], int len) {
////    int i, j, temp;
////    for(i=0;i<len-1;i++) {
////        for(j=i+1;j<len;j++) {
////            if(arr[i]>arr[j]) {
////                temp = arr[i];
////                arr[i]=arr[j];
////                arr[j]=temp;
////            }
////        }
////    }
////}
//
////int lalala() {
////    int arr[] = {22, 34, 4, 54, 67, 2, 87};
////    int len = sizeof(arr)/sizeof(arr[0]);
////    bubble_sort(&arr[0], len);
////    int i;
////    for (i = 0; i < len; i++)
////        printf("%d ", arr[i]);
////    return 0;
////}
//
//
////$$$$$$$$$$$$$$$$$$$$$$$$$$
//void insertion_sort(int *, int);
//
//void insertion_sort(int arr[], int len) {
//    int i, j, temp;
//    for (i = 1; i < len; i++) {
//        temp = arr[i];
////        j=i-1;# 写外面写循环里面是否都一样？？？？？？？
//        for (j = i - 1; j >= 0 && arr[j] > temp; j--) {
//            arr[j + 1] = arr[j];
//        }
//        arr[j + 1] = temp;
//    }
//}
////void insertion_sort(int arr[], int len) {
////    int i,j,temp;
////    for(i=1;i<len;i++) {
////        temp=arr[i];
//////        j = i -1;
////        for(j = i -1;j>=0 && arr[j]>temp;j--) {
////            arr[j+1] = arr[j];
////        }
////    printf ("%d\n", arr[0]);
////    arr[j+1]=temp;
////    }
////}
//
//int lalala() {
//    int arr[] = {22, 34, 4, 54, 67, 2, 87};
////    int arr[] = {1};
//    int len = sizeof(arr) / sizeof(arr[0]);
//    insertion_sort(&arr[0], len);
//    int i;
//    for (i = 0; i < len; i++)
//        printf("%d ", arr[i]);
//    return 0;
//}
//
//void swap1(int x, int y) {
//    int temp;
//    temp = x;
//    x = y;
//    y = temp;
//
//}
//
//void swap2(int *x, int *y) {
//    int temp;
//    temp = *x;
//    *x = *y;
//    *y = temp;
//}
//
////
////void SecondMain(){
////    int a=3, b=4;
//////    swap1(a, b);
//////    swap2(&a, &b);
//////    printf("%d %d",a, b);
////
////    while(true){
////        a-=999999999;
////        if(a>9)
////            break;
////    }
////    printf("End");
////}
//
////#define SIZE 10
////int sump10(int *, int );
////void ARR() {
////    int marbles[SIZE] = {1,2,3,4,5,6,7,8,9,10};
////    long answer;
////    answer=sump10(marbles, SIZE);
////    printf("%ld", answer);
////
////}
////int sump10(int * start, int n) {
////    int total=0;
////    for(int i=0;i<n;i++) {
////        total += start[i];
////    }
////    return total;
////}
//void P() {
//    int num[5] = {100, 200, 300, 400, 500};
//    int *p1, *p2, *p3;
//    p1 = num;
//    p2 = &num[2];
////    p1++;
//    ++p1;
//    printf("%d", *p1);
//}
//
//
//enum spectrum {
//    red, orange, yellow, green, blue, violet
//};
//const char *colors1[] = {"red", "orange", "yellow", "green", "blue", "violet"};
//
//#define LEN 30
//
//int enumFunc() {
//    char choise[LEN];
//    enum spectrum color;
//    bool color_is_found = false;
//
//    puts("Enter a color(empty line to quit):");
//    while (gets(choise) != NULL && choise[0] != '\0') {
//        for (color = red; color <= violet; color++) {
//            if (strcmp(choise, colors1[color]) == 0) {
//                color_is_found = true;
//                break;
//            }
//        }
//        if (color_is_found)
//            switch (color) {
//                case red :
//                    puts("red");
//                    break;
//                case orange :
//                    puts("orange");
//                    break;
//                case yellow:
//                    puts("yellow");
//                    break;
//                case green:
//                    puts("green");
//                    break;
//                case blue:
//                    puts("blue");
//                    break;
//                case violet:
//                    puts("violet");
//                    break;
//            }
//        else
//            printf("~~~~~~~~~");
//        color_is_found = false;
//        puts("Next color, please:");
//    }
//    puts("Goodbye");
//    return 0;
//}

char showmenu(void);

void eatline(void);

void show(void((*fp)(char *)), char *str);

void ToUpper(char *);

void ToLower(char *);

void Transpose(char *);

void Dummy(char *);

int func_p() {
    char line[81];
    char copy[81];
    char choice;
    void (*pfun)(char *);
    puts("Enter a string (empty line to quit):");
    while (gets(line) != NULL && line[0] != '\0') {
        while ((choice = showmenu()) != 'n') {
            switch (choice) {
                case 'u':
                    pfun = ToUpper;
                    break;
                case 'l':
                    pfun = ToLower;
                    break;
                case 't':
                    pfun = Transpose;
                    break;
                case 'o':
                    pfun = Dummy;
                    break;
            }
            strcpy(copy, line);
            show(pfun, copy);
        }
        puts("Enter a string (empty line to quit)");
    }
    puts("Bye!");
    return 0;
}

char showmenu(void) {
    char ans;
    puts("Enter menu choice:");
    puts("u)uppercase l)lowercase t)transposed case o)original case n)next string");
    ans = getchar();
    ans = tolower(ans);
    eatline();
    while (strchr("ulton", ans) == NULL) {
        puts("please enter a u, l, t, o, n:");
        ans = tolower(getchar());
        eatline();
    }
    return ans;

}

void eatline(void) {
    char a = getchar();
    while (a != '\n') {
        printf("%c", a);
        a = getchar();
        continue;
    }

//    while(getchar() != '\n')
//        continue;
}

void ToUpper(char *str) {
    while (*str) {
        *str = toupper(*str);
        str++;
    }
}

void ToLower(char *str) {
    while (*str) {
        *str = tolower(*str);
        str++;
    }
}

void Transpose(char *str) {
    while (*str) {
        if (islower(*str))
            *str = toupper(*str);
        else if (isupper(*str))
            *str = tolower(*str);
        str++;
    }
}

void Dummy(char *str) {

}

void show(void(*fp)(char *), char *str) {
    (*fp)(str);
    puts(str);
}

void test() {
    int a = 5;
    printf("%d\n", a * 3);
    printf("%d\n", a << 1);
    printf("%d\n", ~a);
    printf("%d\n", 5 & 2);
    printf("%d\n", 5 | 2);
    int b = 987654;
    printf("%d\n", b & 3);
    printf("%d", b & 32);

}

void PointerTest() {
    int *p;
    printf("%p\n", p);
    int *p2 = malloc(sizeof(int));
    printf("%p\n", p2);
    int *p3 = NULL;
    printf("%p\n", p3);
    int *p4 = 0;
    printf("%p\n", p4);

}

void test123() {
    char c;
    char array[10];
    int a, i = 0;
    printf("请按照前序遍历的方式依次输入数据:");
//    scanf("%c",&c);
    while(c = getchar()) {
        if(c == '\n')
            break;
        printf("%c", c);
        array[i]=c;
        i++;
    }
    printf("%s", array);

//    while (c = getchar() && c != '\n') {
//        array[i] = c;
//        i++;
//
//    }
//
//    a = strlen(array);

}