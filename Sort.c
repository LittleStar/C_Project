//
// Created by Star on 26/12/16.
//
#include <stdio.h>

int *InsertSort(int *, int);

void RunSort() {
    int array[] = {6, 5, 4, 3, 2, 1};
    int *p = InsertSort(array, 6);
    for (int i = 0; i < 6; i++)
        printf("%d ", p[i]);
}

int *InsertSort(int *array, int n) {
    for (int i = 0; i < n-1; i++)
        for (int j = i + 1; j < n; j++) {
            if (array[i] > array[j]) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
    return array;
}


